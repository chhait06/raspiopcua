#!/bin/bash

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

DOCKER_SRC_DIR=/opt/build_scr

BUILDER_NAME="builder"

cd $SOURCE_DIR

cd docker/build_env

. ./generate_build_image.sh

#echo ok $DOCKER_BUILD_NAME

cd $SOURCE_DIR

#create and start the container
docker run --volume $SOURCE_DIR:$DOCKER_SRC_DIR --workdir $DOCKER_SRC_DIR --user=$(id -u):$(id -g) -it -d --name $BUILDER_NAME $DOCKER_BUILD_NAME


#docker exec $BUILDER_NAME cd build
docker exec -w "$DOCKER_SRC_DIR/build" $BUILDER_NAME echo "$(pwd)"

docker exec -w "$DOCKER_SRC_DIR/build" $BUILDER_NAME cmake ..
#cmake ..

#docker exec -w "$DOCKER_SRC_DIR/build" $BUILDER_NAME ls

docker exec -w "$DOCKER_SRC_DIR/build" $BUILDER_NAME cmake -DCPACK_DEBIAN_PACKAGE_ARCHITECTURE='$(dpkg --print-architecture)' --build . --target raspiopcua

docker exec -w "$DOCKER_SRC_DIR/build" $BUILDER_NAME cpack -G DEB

#temporary diable
docker stop $BUILDER_NAME
docker container rm $BUILDER_NAME






#docker run --rm --volume $SOURCE_DIR:$DOCKER_SRC_DIR  --workdir $DOCKER_SRC_DIR opcua_builder sh build_docker.sh

#docker create --volume $SOURCE_DIR:$DOCKER_SRC_DIR --workdir $DOCKER_SRC_DIR -t --name $BUILDER_NAME $DOCKER_BUILD_NAME
