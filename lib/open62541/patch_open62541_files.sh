#!/bin/bash

python patch_open62541_log_source.py > open62541_new.c
rm open62541.c
mv open62541_new.c open62541.c

python patch_open62541_log_header.py > open62541_new.h
rm open62541.h
mv open62541_new.h open62541.h
