#!/bin/python

import re
import sys




pattern_source_variable_incert  = r"#ifdef __clang__\n__attribute__\(\(__format__\(__printf__,\s3\s,\s0\)\)\)\n#endif\nvoid\nUA_Log_Stdout\("

pattern_source_variable_assignment  = r"    conf->logger = UA_Log_Stdout;\n"


source = 'open62541.c'

str = ""


if len(sys.argv) == 2:
    source= sys.argv[2]
elif len(sys.argv)!=1:
    raise Exception('invalid argument count')
    


##handle source file
with open(source) as f:
    str = f.read()
f.close()

matcher_incert = re.compile(pattern_source_variable_incert)
matcher_assign = re.compile(pattern_source_variable_assignment)


match = matcher_incert.search(str)

if match:
    str = matcher_incert.sub('//patched entpoint for custom logging\nvoid (*UA_Log_Default)(UA_LogLevel level, UA_LogCategory category, const char *msg, va_list args) = UA_Log_Stdout;\n\n#ifdef __clang__\n__attribute__((__format__(__printf__, 3 , 0)))\n#endif\nvoid\nUA_Log_Stdout(', str )
else:
     raise Exception('variable match not found')
 
 
match = matcher_assign.search(str)
 
if match:
    str = matcher_assign.sub('    conf->logger = UA_Log_Default;\n', str )
else:
     raise Exception('incert match not found')


print(str)
