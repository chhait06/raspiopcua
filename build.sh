#!/bin/bash

CURRENT_DIR=$(pwd)

SOURCE_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

BUILD_DIR="$SOURCE_ROOT/build"

cd $SOURCE_ROOT

mkdir -p $BUILD_DIR

cd $BUILD_DIR

rm -rf *

cmake -DBUILD_TEST=ON ..

cmake --build . --target all

cmake --build . --target test


cd $CURRENT_DIR
