#include "Log.h"


#include <cstdarg>
#include <string>

const char* Log::LevelNames[] = { "emerg","alert","crit","err","warning","notice","info","debug" };

#ifndef JOURNALD

std::string Log::fmtStr(const char * format, ...)
{
	static char buffer[256];
	std::va_list args;

	va_start(args, format);
    int n = std::vsnprintf(static_cast<char*>(buffer), sizeof(buffer), format, args);
	va_end(args);

    std::string str(buffer, n);

	return str;
}

#endif



