#ifndef LOG_H
#define LOG_H

#define U8(text) (const_cast<char*>(u8"" text))
#define C_STR(string) (const_cast<char*>(( string ).data()))        //.c_str() == .data()

#ifdef JOURNALD
#include <systemd/sd-journal.h>

#define L_PRIORITY(lvl) "PRIORITY=%d", static_cast<int>(lvl)

#define L_MESSAGE(msg) "MESSAGE=%s", msg
#define L_MESSAGE_F(msg, ...) "MESSAGE=" msg, ##__VA_ARGS__

#define L_UNIT(unitName) "UNIT=%s", unitName
#define L_UNIT_F(unitName, ...) "UNIT=" unitName, ##__VA_ARGS__

#define L_TEST(test) "TEST=%s", test
#define L_TEST_F(test, ...) "TEST=" test, ##__VA_ARGS__

#define L_ELEM(elem_name, content) elem_name "=%s", (content)
#define L_ELEM_F(elem_name, fmt, ...) ( elem_name "=" fmt ) , ##__VA_ARGS__


//#define LOG_F(lvl, msgFmt, ...) sd_journal_send( L_PRIORITY(lvl), L_MESSAGE_F( msgFmt ), ##__VA_ARGS__, NULL)
#define LOG_FN(prio, unit, msg, ...) sd_journal_send( L_PRIORITY(prio) , unit, msg, ##__VA_ARGS__, NULL)

#else
#include <string>

#define L_PRIORITY(lvl) levelToText(lvl) // ::Log::fmtStr("[%s]", levelToText(lvl))

#define L_UNIT(unitName) unitName
#define L_UNIT_F(fmt, ...) L_UNIT(::Log::fmtStr(fmt, ##__VA_ARGS__).c_str())

#define L_MESSAGE(msg) msg
#define L_MESSAGE_F(msg, ...) L_MESSAGE(::Log::fmtStr(msg, ##__VA_ARGS__ ).c_str())

#define L_TEST(test) ::Log::fmtStr("[TEST: %s]", test )
#define L_TEST_F(fmt, ...) L_TEST(::Log::fmtStr(fmt, ##__VA_ARGS__ ).c_str() )

#define L_ELEM(elem_name, content) ::Log::fmtStr("[%s: %s]", (elem_name) , (content) )
#define L_ELEM_F(elem_name, fmt, ...) L_ELEM(elem_name, ::Log::fmtStr(fmt, ##__VA_ARGS__ ).c_str() )

//#define LOF_FN( ...) ::Log::log( L_PRIORITY(lvl), L_MESSAGE_F(msgFmt), __VA_ARGS__ )

#define LOG_FN( prio, unit, message, ... ) ::Log::log( L_PRIORITY(prio), unit, message, ##__VA_ARGS__ )
#endif


#define LOG LOG_FN

//LOG(Log::Level::Notice,
//	L_UNIT_F("main:%s", "startup"), 
//	L_MESSAGE_F("it works%s", "!!!"), 
//	L_TEST_F("ping %s %s", "pong", "ms pacman"), L_TEST_F("marco %s", "polo"));
//

#include <iostream>
#include <iomanip>

namespace Log {	

		enum class Level : int {
			//syslog style level
			Emergency		= 0,	//emerg
			Alert			= 1,	//alert
			Critical		= 2,	//crit
			Error			= 3,	//err
			Warning			= 4,	//warning
			Notice			= 5,	//notice
			Informational	= 6,	//info
			Debug			= 7		//debug
		};

		extern const char* LevelNames[];

		inline const char* levelToText(Level lvl)
		{
            return LevelNames[static_cast<int>(lvl)];
		};

#ifndef JOURNALD
	template<typename PrioT, typename UnitT, typename MessageT, typename ...Args>
	void log(PrioT && prio, UnitT && unit, MessageT && message, Args && ...args)
	{
		std::time_t t = std::time(nullptr);
		std::tm localTm = *std::localtime(&t);	 

		auto logElem = [](auto & x) { std::cout << ' ' << x ; };
		
		std::cout << std::put_time(&localTm, "%b %d %H:%M:%S [") << std::setw(sizeof("warning"))<< std::left<< prio << "] : " << "[" << unit << "] " << message;

		(logElem(args), ...);

		std::cout << '\n';
	}

	std::string fmtStr(const char * format, ...);
#endif		
}
#endif //LOG_H
