#ifndef UASERVICE_H
#define UASERVICE_H

//#include "SignalSlot.h"
#include <ServiceConfig.h>

#include <UaServer.h>
#include <ComponentManager.h>
#include <ConfigLoader.h>
#include "SignalSlot.h"

namespace hsesslingen::opcua {
    class UaService {

    public:
        UaService(const ServiceConfig & cfg);


	public:
        int init();

        int run();

        void stop();

        int close();
			

    private:
       ServiceConfig  config;
       UaServer server;


       ComponentManager compMgr;
       ConfigLoader cfgLoader;

       bool running;

    private:    //slots
       hsesslingen::util::Slot<ComponentConfig&> ignoreFilter;

	};
}
#endif //UASERVICE_H
