#include "UaLogger.h"

#include <Log.h>

#include <cstdarg>
#include <cstdio>
#include <string>

const char *logCategoryNames[7] = { "network", "channel", "session", "server", "client", "userland", "securitypolicy" }; //from open62541.c

static char buffer[512];

::Log::Level uaLogLevelToSysLogLevel(UA_LogLevel lvl)
{

	using L = ::Log::Level;

	switch (lvl)
	{
	case	UA_LOGLEVEL_TRACE:
		return L::Debug;

	case 	UA_LOGLEVEL_DEBUG:
		return L::Debug;

	case 	UA_LOGLEVEL_INFO:
		return L::Informational;

	case 	UA_LOGLEVEL_WARNING:
		return L::Warning;

	case 	UA_LOGLEVEL_ERROR:
		return L::Error;

	case 	UA_LOGLEVEL_FATAL:
		return L::Emergency;

	default:	//should never be used
		return L::Notice;

	}

}

void hsesslingen::opcua::UaLogger(UA_LogLevel level, UA_LogCategory category, const char * msg, va_list args)
{
	int n = std::vsnprintf(buffer, sizeof(buffer), msg, args);

    std::string msgText(buffer, n);

	if (n > sizeof(buffer) - 1)
	{
		msgText.append("...");
	}


	LOG(uaLogLevelToSysLogLevel(level), L_UNIT_F("open62541:%s", logCategoryNames[(int)category]), L_MESSAGE(msgText.c_str()) );

}
