#include "UaService.h"
#include <algorithm>

hsesslingen::opcua::UaService::UaService(const ServiceConfig & cfg)
    : config(cfg)
    , server(config)
    , compMgr(&server)
    , cfgLoader()
    , running(true)
    ,ignoreFilter()
{

    ignoreFilter.setReceiveHandle([this](ComponentConfig& component)
    {
        auto& name =component.getName();
        auto& ignored = config.getIgnoredComponents();
        auto it = std::find(ignored.begin(), ignored.end(), name);
        if(it != ignored.end())
        {
            component.setDisabled(true);
        }
    });

    cfgLoader.ComponentConfigFilterSignal.connect(ignoreFilter);
    cfgLoader.ComponentConfigParsedSignal.connect(compMgr.ParsedComponentConfigSlot);
}

int hsesslingen::opcua::UaService::init(){

    server.init();
    cfgLoader.LoadComponents(config.getPathToComponentFolder());

    compMgr.initComponents();
return 0;
}

int hsesslingen::opcua::UaService::run()
{
    while(running){
        compMgr.iterateComponents();
        server.iterate();
    }
    return 0;

}

void hsesslingen::opcua::UaService::stop()
{

    running = false;
}

int hsesslingen::opcua::UaService::close(){

    compMgr.stopComponents();
    return 0;

}

