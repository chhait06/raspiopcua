#ifndef SIGNALSLOT_H
#define SIGNALSLOT_H

#include <set>
#include <algorithm>
#include  <functional>

namespace hsesslingen {
	namespace util {
		template < typename ... Args > class Slot;
		template < typename ... Args > class Signal;

		template < typename ... Args >
		class Slot
		{
		public:
			Slot()
				:receiving(false)
			{};

			Slot(const Slot<Args...> &) = delete;

			~Slot() {

				for (auto it = transmitters.begin(), next = it;
					it != transmitters.end();
					it = next)
				{
                    //iterator auf nächtes objekt schieben, da das aktuelle gleich gelöscht wird
					next++;
					(*it)->disconnect(*this);
				}
			};

			void setReceiveHandle(std::function< void(Args & ...) > lambda) {
				receiveFn = lambda;
			}

			int connect(Signal<Args...> & transmitter) {

				if (transmitters.find(&transmitter) == transmitters.end()) {
					transmitters.insert(&transmitter);
					transmitter.connect(*this);
					return 0;
				}
				else {
					return 1;
				}
			}
			int disconnect(Signal<Args...> & transmitter) {

				if (isReceiving()) {
					throw "Signal emitting at the moment. Disconnect not possible.";
				}

				if (auto it = transmitters.find(&transmitter); it != transmitters.end()) {
					auto t = *it;
					if (t->isEmitting()) { throw "Slot receiving at the moment. Disconnect not possible."; }
					transmitters.erase(it);
					t->disconnect(*this);
					return 0;
				}
				else {
					return 1;
				}

			}

			void receive(Args & ...data) {

				if (receiveFn) {
					receiveFn(data...);
				}
			}

			inline bool isReceiving() { return receiving; }

		protected:
		private:
			std::set< Signal< Args... > * > transmitters;
			bool receiving;
			std::function< void(Args & ...) >  receiveFn;
		};

		template < typename ... Args >
		class Signal
		{
		public:
			Signal()
				:emitting(false)
			{};

			Signal(const Signal< Args... > &) = delete;

			~Signal() {

				for (auto it = receivers.begin(), next = it;
					it != receivers.end();
					it = next)
				{
                    //iterator auf nächtes objekt schieben, da das aktuelle gleich gelöscht wird
					next++;
					(*it)->disconnect(*this);
				}
			};

			void emit(Args & ...data) {

				emitting = true;
				for (Slot< Args... > * rx : receivers)
				{
					rx->receive(data...);
				}
				emitting = false;
			}

			int connect(Slot< Args... > & receiver) {

				if (receivers.find(&receiver) == receivers.end()) {
					receivers.insert(&receiver);

					receiver.connect(*this);
					return 0;
				}
				else {
					return 1;
				}
			}

			int disconnect(Slot< Args... > & receiver) {

				if (isEmitting()) {
					throw "Signal emitting at the moment. Disconnect not possible.";
				}

				if (auto it = receivers.find(&receiver); it != receivers.end()) {
					auto r = *it;
					if (r->isReceiving()) { throw "Slot receiving at the moment. Disconnect not possible."; }
					receivers.erase(it);
					r->disconnect(*this);
					return 0;
				}
				else {
					return 1;
				}
			}

			inline bool isEmitting() { return emitting; }

		protected:
		private:
			std::set< Slot< Args... > * > receivers;
			bool emitting;
		};
	}
}

#endif //SIGNALSLOT_H
