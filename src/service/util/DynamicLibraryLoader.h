#ifndef DYNAMICLIBRARYLOADER_H
#define DYNAMICLIBRARYLOADER_H


#include <filesystem>

namespace hsesslingen::util
{
    class DynamicLibraryLoader
    {
    public:
        static void* loadDynamicLibrary(const std::filesystem::path& libraryPath);

        static int closeDynamicLibrary(void* libraryHandle);

        static void* loadSymbol(const void* libraryHandle, const char* symbolName);
    };
}

#endif // !DYNAMICLIBRARYLOADER_H

/* Infos
//	
//	
//	Linux:		http://tldp.org/HOWTO/Program-Library-HOWTO/dl-libraries.html
//	Widnows:
*/
