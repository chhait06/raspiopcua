﻿#include "DynamicLibraryLoader.h"

//Plattformabhängige loader header
#ifdef _WIN32	//Windows

#include <windows.h>
#include <iostream>


#elif __GNUC__	//Unix

#include <dlfcn.h>

#else	//other

//not supported at the moment

#endif //loader header

//#include <iostream>
//using namespace hsesslingen::util;
#include <filesystem>

namespace fs = std::filesystem;
void* hsesslingen::util::DynamicLibraryLoader::loadDynamicLibrary(const std::filesystem::path& libraryPath)
{
    void* handle{nullptr};

    if( ! fs::is_regular_file(libraryPath) )
    {
        throw fs::filesystem_error(
                    "libraryPath is not found or not a regular file",
                    libraryPath,
                    std::make_error_code(std::errc::no_such_file_or_directory)
                    );
    }


#ifdef _WIN32	//Windows
    {
		//std::wstring(libraryPath.begin(), libraryPath.end()).c_str()
        HMODULE library = LoadLibraryW(	fs::absolute(libraryPath).wstring().c_str() );	//unicode string to wide char used by windows functions
        if (library == NULL) {
            unsigned int err = GetLastError();

            std::cerr << "ERROR: Loading library located at \"" << libraryPath << "\" not posible. Widnows function  LoadLibrary throwd: 0x" << std::hex << err << std::endl;

        }
        else {
            handle = library;

            std::clog << "Loaded library located at \"" << libraryPath << "\" successfully" << std::endl;
        }
    }
#elif __GNUC__	//Unix

    {
        void* library {nullptr};
        char* err{nullptr};

        dlerror();	//clear old errors
        library = dlopen(libraryPath.c_str(), RTLD_NOW|RTLD_LOCAL);//RTLD_NOW|RTLD_GLOBAL);//RTLD_LAZY);
        err= dlerror();

        if (err) {
            throw std::runtime_error(std::string(err));
        }
        else {
            handle = library;
        }
    }

#else	//Other

    //not supported at the moment

#endif

    return handle;
}

//0 on success, otherwise not equal zero
int hsesslingen::util::DynamicLibraryLoader::closeDynamicLibrary(void* libraryHandle) {
    int freed = 0;

    if (libraryHandle == nullptr) {
        return 0;
    }

#ifdef _WIN32	//Windows

    freed = FreeLibrary((HMODULE)libraryHandle);

    if (!freed)
    {
        //something went wrong
        unsigned int err = GetLastError();
        std::cerr << "ERROR: Freeing library not posible. Widnows function FreeLibrary throwd: 0x" << std::hex << err << std::endl;
        freed = (int)err;
    }

#elif __GNUC__	//Unix

    dlerror(); //clear old errors

    freed = dlclose(libraryHandle);

    char* err = dlerror();
    if(err)	//if (!freed )
    {
        //something went wrong

        //ignore it. doesnt matter
        //throw std::runtime_error(std::string(err));
       // std::cerr << "ERROR: Freeing library not posible. Unix function dlclose throwd: " << err << std::endl;
    }
    else
    {
       // std::clog << "Closed library with handle\"" << std::hex  << libraryHandle << "\" successfully" << std::endl;
    }


#else	//Other
    //not supported at the moment
    freed = ~0;

#endif
    return freed;
}

//0 on success
void* hsesslingen::util::DynamicLibraryLoader::loadSymbol(const void* libraryHandle, const char* symbolName)
{
    void* symbolPtr = nullptr;
#ifdef _WIN32

    symbolPtr = GetProcAddress((HMODULE)libraryHandle, symbolName);
    if (!symbolPtr)
    {
        //something went wrong
        unsigned int err = GetLastError();
        std::cerr << "ERROR: Loadinfg symbol \"" << symbolName << "\" not posible. Widnows function GetProcAddress throwd: 0x" << std::hex << err << std::endl;
        throw "symbol not found";
    }

#elif __GNUC__

    dlerror(); // clear old errors

    symbolPtr = dlsym(const_cast<void*>(libraryHandle), symbolName);

    char* err = dlerror();
    if ( err ) {
       // std::cerr << "ERROR: Loading symbol \"" << symbolName << "\" not posible. Widnows function GetProcAddress throwd: 0x" << std::hex << err << std::endl;
        throw std::runtime_error(std::string(err));
    }
    else {
        //std::clog << "Loaded symbol \"" << symbolName << "\" from library with handle \"" << std::hex << libraryHandle << "\" successfully"<< std::endl;
    }

#else	//other

    //not supported at the moment
    ret = ~0;

#endif
    return symbolPtr;

}

/*
#ifdef _WIN32

//Widnows

#elif __GNUC__

//Unix

#else

//other

#endif
*/



