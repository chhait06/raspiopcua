#ifndef ICONFIGPARSER_H
#define ICONFIGPARSER_H

#include <string>
#include <filesystem>
#include <memory>
#include <vector>

namespace hsesslingen::opcua
{
class IConfigParser{};

class IServiceConfigParser : public IConfigParser
{
public:
    //mandatory
    virtual std::string getCertificatePath() = 0;
    virtual std::string getComponentConfigDirectory() = 0;
    virtual short getPort() = 0;

    //optional
    virtual std::vector<std::string> getIgnoredComponents() { return std::vector<std::string>{"testComponent"};  };
public:
    virtual ~IServiceConfigParser(){};
};

class IComponentConfigParser : public IConfigParser{
public:
    //mandatory
    virtual std::string getComponentName() = 0;
    virtual std::string getBinaryLocation() = 0;

    //optional
    virtual bool getDisabled(){ return false; };

public:
    virtual ~IComponentConfigParser(){};
};


class IParserGeneratorBase
{
public:
    virtual ~IParserGeneratorBase(){};
};


class IComponentParserGeneratorBase : public IParserGeneratorBase
{
public:
    virtual std::unique_ptr<IComponentConfigParser> CreateInstance(const std::filesystem::path& pathToConfig) = 0;
    virtual ~IComponentParserGeneratorBase(){};
};

template<class ConfigParserType>
class IComponentConfigParserGenerator : public IComponentParserGeneratorBase{
public:
    std::unique_ptr<IComponentConfigParser> CreateInstance(const std::filesystem::path& pathToConfig) override
    {
        return std::make_unique<ConfigParserType>(pathToConfig);
    };
    ~IComponentConfigParserGenerator() {};
};


class IServiceParserGeneratorBase : public IParserGeneratorBase
{
public:
    virtual std::unique_ptr<IServiceConfigParser> CreateInstance(const std::filesystem::path& pathToConfig) = 0;
    virtual ~IServiceParserGeneratorBase(){};
};

template<class ConfigParserType>
class IServiceConfigParserGenerator : public IServiceParserGeneratorBase{
public:
    std::unique_ptr<IServiceConfigParser> CreateInstance(const std::filesystem::path& pathToConfig) override
    {
        return std::make_unique<ConfigParserType>(pathToConfig);
    };
    ~IServiceConfigParserGenerator() {};
};
}

#endif //ICONFIGPARSER_H
