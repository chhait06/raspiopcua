#ifndef COMPONENTCONFIGPARSER_H
#define COMPONENTCONFIGPARSER_H

#include <string>
#include "ComponentConfig.h"
#include <map>
#include <memory>
#include <filesystem>
#include <IConfigParser.h>

namespace hsesslingen {
	namespace opcua {
		class ComponentConfigParser {
		public:
            ComponentConfigParser() = delete;
            ~ComponentConfigParser() = delete;

            static ComponentConfig ParseConfig(const std::filesystem::path &configFile);
            static bool isParserAvailabe(const std::filesystem::path& configFile);

        private:
           static std::unique_ptr<IComponentConfigParser> getComponentConfigParser(const std::filesystem::path& configFile);
           static const std::map<const std::string, std::shared_ptr<IComponentParserGeneratorBase>> ComponentConfigParsers;
		};
	}
}
#endif //COMPONENTCONFIGPARSER_H
