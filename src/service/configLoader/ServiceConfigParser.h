#ifndef SERVICECONFIGPARSER_H
#define SERVICECONFIGPARSER_H

#include <filesystem>
#include <map>
#include <string>
#include "IConfigParser.h"
#include "ServiceConfig.h"

namespace hsesslingen::opcua
{

class ServiceConfigParser {
public:
    ServiceConfigParser();
    ~ServiceConfigParser();

    static ServiceConfig ParseConfig(const std::filesystem::path &configFile);
    static bool isParserAvailabe(const std::filesystem::path& configFile);

private:
    static std::unique_ptr<IServiceConfigParser> getServiceConfigParser(const std::filesystem::path& configFile);
    static const std::map<const std::string, std::shared_ptr<IServiceParserGeneratorBase>> ServiceConfigParsers;
};

}
#endif //SERVICECONFIGPARSER_H

