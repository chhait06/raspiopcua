#include "ComponentConfigParser.h"

#include "parsers/TomlComponentParser.h"

namespace hsesslingen::opcua {

const std::map<const std::string, std::shared_ptr<IComponentParserGeneratorBase>>
ComponentConfigParser::ComponentConfigParsers
{
    {
        ".ini",
        std::make_shared<IComponentConfigParserGenerator<TomlComponentParser>>()
    },
    {
        ".toml",
        std::make_shared<IComponentConfigParserGenerator<TomlComponentParser>>()
    }
};


bool ComponentConfigParser::isParserAvailabe(const std::filesystem::path& configFile)
{
   return ComponentConfigParsers.find(configFile.extension().u8string()) != ComponentConfigParsers.end();
}


std::unique_ptr<IComponentConfigParser> ComponentConfigParser::getComponentConfigParser(const std::filesystem::path& configFile)
{
    return ComponentConfigParsers.find( configFile.extension().u8string())->second->CreateInstance(configFile);
}

hsesslingen::opcua::ComponentConfig ComponentConfigParser::ParseConfig(const std::filesystem::path &configFile)
{
    auto parser = getComponentConfigParser(configFile);

    ComponentConfig c{configFile.u8string()};

    c.setName(parser->getComponentName());
    c.setPathToBin(parser->getBinaryLocation());
    c.setDisabled(parser->getDisabled());

    return c;
}

}

