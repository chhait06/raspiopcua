#include "ConfigLoader.h"

#include <filesystem>
#include <utility>

#include "IConfigParser.h"


#include "Log.h"

namespace fs = std::filesystem;

using namespace hsesslingen::opcua;

hsesslingen::opcua::ConfigLoader::ConfigLoader()
{
}


hsesslingen::opcua::ConfigLoader::~ConfigLoader()
{

}

void hsesslingen::opcua::ConfigLoader::LoadComponents(const std::string & directory) {
    for (auto& file : fs::directory_iterator(directory))
    {
        try{

            ComponentConfig c = LoadComponent(file.path());//LoadComponent(p.generic_u8string());

            bool disabledByConfig = c.getDisabled();

            ComponentConfigFilterSignal.emit(c);

            if( ! c.getDisabled())
            {
                ComponentConfigParsedSignal.emit(c);
            }
            else
            {

                LOG(Log::Level::Notice,
                    L_UNIT("ConfigLoader:Component"),
                    L_MESSAGE_F("Parsed ComponentConfig '%s' successfully, but disabled by %s.",
                                C_STR(c.getName()), (disabledByConfig? "itself" : "service policy" )),
                    L_ELEM("CONFIG_LOCATION", C_STR(fs::absolute(file.path()).u8string()))
                    // L_ELEM_F("LOCATION", "<working directory>/%s", C_STR(file.path().u8string()))
                    );
            }
        }catch(...){
            //not a valid component
        }
    }
}

hsesslingen::opcua::ServiceConfig ConfigLoader::LoadService(const std::filesystem::path &configFile)
{
    if ( ! (fs::is_regular_file(configFile) && ServiceConfigParser::isParserAvailabe(configFile)) )
    {
        throw std::exception();
    }
    return ServiceConfigParser::ParseConfig(configFile);
}

hsesslingen::opcua::ComponentConfig ConfigLoader::LoadComponent(const std::filesystem::path & configFile)
{
    if ( ! (fs::is_regular_file(configFile) && ComponentConfigParser::isParserAvailabe(configFile)) )
    {
        throw std::exception();
    }
    return ComponentConfigParser::ParseConfig(configFile);
}

