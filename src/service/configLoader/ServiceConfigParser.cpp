#include "ServiceConfigParser.h"

#include "parsers/TomlServiceParser.h"

using namespace hsesslingen::opcua;

const std::map<const std::string, std::shared_ptr<IServiceParserGeneratorBase>>
ServiceConfigParser::ServiceConfigParsers
{
    {
        ".ini",
        std::make_shared<IServiceConfigParserGenerator<TomlServiceParser>>()
    },
    {
        ".toml",
        std::make_shared<IServiceConfigParserGenerator<TomlServiceParser>>()
    }
};

ServiceConfig ServiceConfigParser::ParseConfig(const std::filesystem::path &configFile)
{
    auto parser = getServiceConfigParser(configFile);

    ServiceConfig c;

    c.setPathToCertificate(parser->getCertificatePath());
    c.setPathToComponentFolder(parser->getComponentConfigDirectory());
    c.setPort(parser->getPort());
    auto ignored = parser->getIgnoredComponents();
    c.getIgnoredComponents().assign(ignored.begin(), ignored.end());

    return c;

}

bool ServiceConfigParser::isParserAvailabe(const std::filesystem::path &configFile)
{
    return ( ServiceConfigParsers.find(configFile.extension().u8string() ) != ServiceConfigParsers.end() );
}

std::unique_ptr<IServiceConfigParser> ServiceConfigParser::getServiceConfigParser(const std::filesystem::path &configFile)
{
    return ServiceConfigParsers.find( configFile.extension().u8string() )->second->CreateInstance(configFile);
}


