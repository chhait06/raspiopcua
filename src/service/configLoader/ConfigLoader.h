﻿#ifndef CONFIGLOADER_H
#define CONFIGLOADER_H

#include "ComponentConfigParser.h"
#include "ServiceConfigParser.h"

#include "ComponentConfig.h"
#include "ServiceConfig.h"

#include "SignalSlot.h"

#include <string>
#include <string_view>
#include <map>
#include <memory>

namespace hsesslingen::opcua {

class ConfigLoader {
public:
    ConfigLoader();
    ~ConfigLoader();

    void LoadComponents(const std::string &directory);
    ServiceConfig LoadService(const std::filesystem::path &configFile);

private:
    ComponentConfig LoadComponent(const std::filesystem::path & configFile );

   // std::unique_ptr<IComponentConfigParser> getComponentConfigParser(const std::filesystem::path& configFile);
public:
    //Signal

    //emits before the 'ComponentConfigParsedSignal' making changes to the Config posible. e.g. disable the config (config.setDisabled(true)) or enforcing namepolicys (yust an idea)
    hsesslingen::util::Signal<ComponentConfig&> ComponentConfigFilterSignal;

    //emits the Config to an endpoint handling it from now on (sould be ComponentManager)
    hsesslingen::util::Signal<ComponentConfig>	ComponentConfigParsedSignal;
};

}
#endif // !CONFIGLOADER_H
