#include "TomlComponentParser.h"

hsesslingen::opcua::TomlComponentParser::TomlComponentParser(const std::filesystem::path &pathToConfig)
    :configPath(pathToConfig)
    ,config(cpptoml::parse_file(std::filesystem::absolute(pathToConfig).u8string()))
{

}

hsesslingen::opcua::TomlComponentParser::~TomlComponentParser()
{

}

std::string hsesslingen::opcua::TomlComponentParser::getComponentName()
{
    auto name = config->get_qualified_as<std::string>("component.name");

    if( ! name)
    {
        throw std::exception();
    }

    return *name;
}

std::string hsesslingen::opcua::TomlComponentParser::getBinaryLocation()
{
    auto binaryLocation = config->get_qualified_as<std::string>("component.pathToBin");

    if( ! binaryLocation)
    {
        throw std::exception();
    }

    return *binaryLocation;
}

bool hsesslingen::opcua::TomlComponentParser::getDisabled()
{
    auto disabled = config->get_qualified_as<bool>("component.disabled");



    return disabled.value_or( IComponentConfigParser::getDisabled() );
}
