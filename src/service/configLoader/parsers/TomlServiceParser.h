#ifndef TOMLCOMPONENTPARSER_H
#define TOMLCOMPONENTPARSER_H

#include "IConfigParser.h"
#include <string>

#include <cpptoml.h>


namespace hsesslingen::opcua
{

class TomlServiceParser:public IServiceConfigParser
{
public:
    TomlServiceParser(const std::filesystem::path& pathToConfig);
    ~TomlServiceParser();

public:
    std::string getCertificatePath() override;
    std::string getComponentConfigDirectory() override;
    short getPort() override;
    std::vector<std::string> getIgnoredComponents() override;

private:
    std::filesystem::path configPath;
    std::shared_ptr<cpptoml::table> config;
};

}


#endif //TOMLCOMPONENTPARSER_H
