#include "TomlServiceParser.h"

hsesslingen::opcua::TomlServiceParser::TomlServiceParser(const std::filesystem::path &pathToConfig)
    :configPath(pathToConfig)
    ,config(cpptoml::parse_file(std::filesystem::absolute(pathToConfig).u8string()))
{

}

hsesslingen::opcua::TomlServiceParser::~TomlServiceParser()
{

}

std::string hsesslingen::opcua::TomlServiceParser::getCertificatePath()
{
    auto cert = config->get_qualified_as<std::string>("service.cert");

    if( ! cert)
    {
        throw std::exception();
    }

    return *cert;
}

std::string hsesslingen::opcua::TomlServiceParser::getComponentConfigDirectory()
{
    auto confLocation = config->get_qualified_as<std::string>("service.compDir");

    if( ! confLocation)
    {
        throw std::exception();
    }

    return *confLocation;
}

short hsesslingen::opcua::TomlServiceParser::getPort()
{
    auto port = config->get_qualified_as<short>("service.port");

    if( ! port)
    {
        throw std::exception();
    }

    return *port;
}

std::vector<std::string> hsesslingen::opcua::TomlServiceParser::getIgnoredComponents()
{
    auto ignored = config->get_qualified_array_of<std::string>("service.ignoredComponents");

    return ignored.value_or(IServiceConfigParser::getIgnoredComponents());
}


