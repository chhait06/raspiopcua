#ifndef TOMLCOMPONENTPARSER_H
#define TOMLCOMPONENTPARSER_H

#include "IConfigParser.h"
#include <string>

#include <cpptoml.h>



namespace hsesslingen::opcua
{
class TomlComponentParser:public IComponentConfigParser
{
public:
    TomlComponentParser(const std::filesystem::path& pathToConfig);
    ~TomlComponentParser();

public:
    std::string getComponentName() override;
    std::string getBinaryLocation() override;
    bool getDisabled() override;

private:
    std::filesystem::path configPath;
    std::shared_ptr<cpptoml::table> config;
};
}


#endif //TOMLCOMPONENTPARSER_H
