#ifndef UALOGGER_H
#define UALOGGER_H

#include <open62541.h>

namespace hsesslingen::opcua
{
	extern "C"
		void UaLogger(UA_LogLevel level, UA_LogCategory category, const char *msg, va_list args);

}
#endif //UALOGGER_H


