#ifndef COMPONENTLOADER_H
#define COMPONENTLOADER_H

#include <string>
#include <SignalSlot.h>
#include <map>

#include <Component.h>
#include <UaServer.h>

namespace hsesslingen::opcua {

class ComponentManager {
public :
    ComponentManager(hsesslingen::opcua::UaServer* serverObject);
    ~ComponentManager();

    int initComponents();
    int iterateComponents();
    int stopComponents();

    hsesslingen::util::Slot<ComponentConfig> ParsedComponentConfigSlot;

public:
    std::vector<std::string> getComponentNames();

private:
    std::map<std::string,Component> components;
    hsesslingen::opcua::UaServer * server;
};

}

#endif // !COMPONENTLOADER_H
