#include "Component.h"

#include <DynamicLibraryLoader.h>

#include <utility>

#include <iostream>

using DLL = ::hsesslingen::util::DynamicLibraryLoader;

#define DEBUG_OUT(...)
#ifndef DEBUG_OUT
#define DEBUG_OUT(...) (std::cout << __VA_ARGS__ << "\n" )
#endif

inline ComponentApi getApiHandle(const void* library_handle)
{
    auto api = (reinterpret_cast<GetComponentApiHandleFn>(DLL::loadSymbol(library_handle, API_HANDLE_SYMBOL_NAME)))();
    return api;
}

hsesslingen::opcua::Component::Component(const ComponentConfig& conf, UaServer* const srv)
    : config(conf)
    , binaryHandle(DLL::loadDynamicLibrary(config.getPathToBin()))
    , server(srv)
    , component(ComponentHandle{
                (server == nullptr) ? nullptr : server->getInnerServerHandle(),
                getApiHandle(binaryHandle),
                nullptr,
                this->config.getConfigLocation().c_str()
                })
    ,state{Component::State::New}
{
    DEBUG_OUT( this->config.getName() <<" : ctor" );
}


hsesslingen::opcua::Component::Component(const hsesslingen::opcua::Component &comp)
    :Component(comp.config, comp.server)
{
    DEBUG_OUT( this->config.getName() <<" : copy ctor" );
}

hsesslingen::opcua::Component::~Component()
{
    DEBUG_OUT( this->config.getName() <<" : dtor" );
    Delete();

    if(binaryHandle != nullptr)  {
        DLL::closeDynamicLibrary(binaryHandle);
        binaryHandle=nullptr;
    }
}

hsesslingen::opcua::Component& hsesslingen::opcua::Component::operator=(hsesslingen::opcua::Component && comp)
{
    DEBUG_OUT( this->config.getName() <<" : move=" );

    std::swap(this->config, comp.config);
    std::swap(this->binaryHandle, comp.binaryHandle);
    std::swap(this->server, comp.server);
    std::swap(this->component, comp.component);
    std::swap(this->state, comp.state);

    //in case of small buffer optimization (most cases: string smaler than 16bytes)
    this->component.config_file = config.getConfigLocation().c_str();

    return *this;
}

const hsesslingen::opcua::ComponentConfig& hsesslingen::opcua::Component::getConfig() {
    return config;
}

ComponentApi& hsesslingen::opcua::Component::api()
{
    return component.api;;
}

void hsesslingen::opcua::Component::setUaServer(hsesslingen::opcua::UaServer* const srv)
{
    server = srv;
    component.server = server->getInnerServerHandle();
}

hsesslingen::opcua::Component::State hsesslingen::opcua::Component::current_state()
{
    return this->state;
}

bool hsesslingen::opcua::Component::stoppable()
{
 return state >= State::Started && state <= State::Working;
}





int hsesslingen::opcua::Component::Init()
{
    DEBUG_OUT( this->config.getName() <<" : init" );

    if( ! (state == Component::State::New) )
    {

        DEBUG_OUT(  this->config.getName() <<" : " <<  std::dec << (int)state << " -> init" );
        throw wrong_state_exception();
    }

    int ret=0;
    if(component.api.initFn !=  nullptr )
    {
        ret=component.api.initFn(&component);
    }

    state = State::Initialized;
    return ret;
}

int hsesslingen::opcua::Component::AddNotes()
{
    DEBUG_OUT( this->config.getName() <<" : addnotes" );

    if( ! ( state == Component::State::Initialized) )
    {

        DEBUG_OUT(  this->config.getName() <<" : " << std::dec << (int)state << " -> nodes" );
        throw wrong_state_exception();
    }

    int ret=0;
    if(component.api.nodesFn !=  nullptr )
        ret=component.api.nodesFn(&component);

    state=State::NodesAdded;
    return ret;
}

int hsesslingen::opcua::Component::Start()
{
    DEBUG_OUT( this->config.getName() <<" : start" );

    if(! (state == Component::State::NodesAdded ||  state == Component::State::Stoped) )
    {

        DEBUG_OUT( this->config.getName() <<" : " <<  std::dec << (int)state << " -> start" );
        throw wrong_state_exception();
    }

    int ret=0;
    if(component.api.startFn !=  nullptr )
        ret= component.api.startFn(&component);

    state= Component::State::Started;
    return ret;
}

int hsesslingen::opcua::Component::Iterate()
{
    //DEBUG_OUT( "iter" )

    if(! (state == Component::State::Started
          || state == Component::State::Working) )
    {
        DEBUG_OUT(  this->config.getName() <<" : " << std::dec << (int)state << " -> iter" );
        throw wrong_state_exception();
    }
    state = Component::State::Working;

    int ret=0;
    if(component.api.iterateFn !=  nullptr )
        ret=component.api.iterateFn(&component);

    return ret;
}

int hsesslingen::opcua::Component::Stop()
{
    DEBUG_OUT( this->config.getName() <<" : stop" );

    if(state == Component::State::Stoped){
        return 0;
    }

    if( !( state == Component::State::Started
           || state == Component::State::Working) )
    {

        DEBUG_OUT(  this->config.getName() <<" : " << std::dec << (int)state << " -> stop" );
        throw wrong_state_exception();
    }

    int ret=0;
    if(component.api.stopFn !=  nullptr )
        ret=component.api.stopFn(&component);

    state = Component::State::Stoped;

    return ret;
}

int hsesslingen::opcua::Component::Delete()
{
    DEBUG_OUT( this->config.getName() <<" : del" );
    if( (state >= Component::State::Started
         && state < Component::State::Stoped) )
    {

        DEBUG_OUT( this->config.getName() <<" : " << std::dec << (int)state << " -> stop() -> del" );
        Stop();
    }
    //    if(state != Component::State::Stoped)
    //    {
    //        throw wrong_state_exception();
    //    }
    int ret=0;
    if(component.api.deleteFn !=  nullptr )
        ret=component.api.deleteFn(&component);

    state = Component::State::Deleted;
    return ret;
}
