#include "ComponentManager.h"

#include <DynamicLibraryLoader.h>
#include <vector>

#include <algorithm>
#include "Log.h"
#include <filesystem>
namespace fs = std::filesystem;



hsesslingen::opcua::ComponentManager::ComponentManager(UaServer * serverObject)
    : server(serverObject)
{
    //This is where the magic happens
    ParsedComponentConfigSlot.setReceiveHandle([this](hsesslingen::opcua::ComponentConfig& data)
    {
        try{
            this->components.insert(std::pair<std::string,Component>(data.getName(), Component(data, server) ));

            LOG(Log::Level::Notice,
                L_UNIT("ComponentManager:CreateComponent"),
                L_MESSAGE_F("Load Component '%s' successfully", C_STR(data.getName()))
                );

        }
        catch(std::system_error& e){
            LOG(Log::Level::Notice,
                L_UNIT("ComponentManager:CreateComponent"),
                L_MESSAGE_F("library not available. \"%s\"", e.what())
                );
        }
        catch(std::runtime_error& e){
            LOG(Log::Level::Notice,
                L_UNIT("ComponentManager:CreateComponent"),
                L_MESSAGE_F("symbol not found. \"%s\"", e.what())
                );
        }
    });


    /*
     *configLoad emits config to ComponentManager
     *
     * ComponentMgr creates component out of Config
     * Cmgr safes Component into a storage thing
     *
     * Tests verifies that all components are stored in the storage by getting all components and printing their names.
     *
     *
     *
     *
      this->Components.insert(std::pair<std::string,Component>(data.getName(), Component(data, server) ));

      anonymesObject = Component(data, server)
      funktion(Component Object)

      VOrher:
      Init COnstructo
      Copy Constructor -> Zeitaufwendig

      Nacher:
      Init Constructor
      Move Constructor -> nicht so Zeitaufwendig
     */

}

hsesslingen::opcua::ComponentManager::~ComponentManager()
{
    stopComponents();
    //Component.Delet() ist nicht nötig. destruktor erledigt dies.
}


int hsesslingen::opcua::ComponentManager::initComponents() {
    int ret = 0;

    for(auto& pair : components)
    {
        const std::string& name = pair.first;
        Component& cmp = pair.second;

        cmp.Init();
        cmp.AddNotes();
        cmp.Start();
    }

    return ret;

}
int hsesslingen::opcua::ComponentManager::iterateComponents() {
    int ret = 0;

    for(auto& pair : components)
    {
        const std::string& name = pair.first;
        Component& cmp = pair.second;

        cmp.Iterate();
    }

    return ret;
}

int hsesslingen::opcua::ComponentManager::stopComponents()
{
    int countNotStop = 0;
    for(auto& pair : components)
    {
        const std::string& name = pair.first;
        Component& cmp = pair.second;

        if(cmp.stoppable() && cmp.Stop())
        {
            countNotStop++;
        }
    }
    return countNotStop;

}

std::vector<std::string> hsesslingen::opcua::ComponentManager::getComponentNames()
{
    std::vector<std::string> compNames;

    for(auto& pair : components)
    {
        compNames.push_back(pair.first);
    }

    return compNames;
}

