#ifndef COMPONENT_H
#define COMPONENT_H

#include <string>

#include <component_handle.h>

#include <ComponentConfig.h>
#include <UaServer.h>

#include <exception>

namespace hsesslingen::opcua
{
    class Component
    {
        enum class State
        {

        /*
         *    /-------------------------------------------------------------------------------\
         *    |                                                                               |
         *    |   O                                            _______________________        |
         *    |   |                                           /                       \       |
         *    |   v                                          v                         \      |
         *    | (new) -> (Initialized) -> (NodesAdded) -> (Started) -> (Working) -> (Stoped) -+- -> (Deleted)
         *    |                                              \           \   ^         ^      |
         *    |                                               \           \_/         /       |
         *    |                                                \_____________________/        |
         *    |                                                                               |
         *    \-------------------------------------------------------------------------------/
         *
         * if state is (started) or (working) , (stoped) is gearantied to be passed before (deleded)
         *
         * */

            Deleted = -1,

            New = 0,
            Initialized = 1,
            NodesAdded = 2,
            Started = 3,
            Working=4,
            Stoped = 5
        };
        class wrong_state_exception : std::exception{};

	public:
        Component(const ComponentConfig& conf, UaServer* const srv = nullptr);

      //  Component();
        Component(const Component& comp) ;

        ~Component();

        Component& operator=( Component && comp);

    public:
        const ComponentConfig& getConfig();
        void setUaServer(hsesslingen::opcua::UaServer* const srv);

        Component::State current_state();

        bool stoppable();

        ComponentApi& api();

        int Init();
        int AddNotes();
        int Start();
        int Iterate();
        int Stop();

    private:
        int Delete();


	private:

        Component::State state;

        ComponentConfig config;
        void* binaryHandle;
        UaServer* server;
        ComponentHandle component;
	};
}

#endif //COMPONENT_H
