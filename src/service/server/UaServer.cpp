#include "UaServer.h"
#include <string>

using namespace hsesslingen::opcua;

//int hsesslingen::opcua::UaServer::
UA_Server* loadUaServer(ServerConfig& conf) {

    auto uaCfg = conf.getUaServerConfig();

    UA_Server* server = UA_Server_new(uaCfg);

    return server;
}

hsesslingen::opcua::UaServer::UaServer(const ServerConfig & conf)
    : serverConfig(conf)
    , server(loadUaServer(serverConfig))
{

}

UA_Server * hsesslingen::opcua::UaServer::getInnerServerHandle() {
	return server;
}

hsesslingen::opcua::UaServer::~UaServer() {
	if (server != nullptr) {
		UA_Server_delete(server);
	}
}

ServerConfig& hsesslingen::opcua::UaServer::getServerConfig() {

	if (serverConfig.getUaServerConfig() == nullptr) {
		throw std::string( "UaServerConfig could not be generated");
	}

	return serverConfig;
}

void UaServer::init(){
    UA_StatusCode retval = UA_Server_run_startup(server);
    if(retval != UA_STATUSCODE_GOOD)
    {
        throw retval;
    }
}

void UaServer::iterate()
{
    UA_Server_run_iterate(server, true);
}

int UaServer::shutdown(){
    return UA_Server_run_shutdown(server);
}

