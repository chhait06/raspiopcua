#ifndef UASERVER_H
#define UASERVER_H

#include <open62541.h>
#include <ServerConfig.h>

namespace hsesslingen::opcua {
class UaServer {

public:
    UaServer(const ServerConfig & conf);
    ~UaServer();

    ServerConfig & getServerConfig();

    void init();

    void iterate();

    int shutdown();


    UA_Server * getInnerServerHandle();

private:
    //int loadUaServer();

    ServerConfig serverConfig;
    UA_Server * server;
};

}
#endif //UASERVER_H
