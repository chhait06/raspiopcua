#include "ComponentConfig.h"


hsesslingen::opcua::ComponentConfig::ComponentConfig(std::string configPath)
    : configLocation{configPath}
{

}

hsesslingen::opcua::ComponentConfig::ComponentConfig(const hsesslingen::opcua::ComponentConfig &config)
    :configLocation{config.configLocation}
    ,name(config.name)
    ,pathToBin(config.pathToBin)
    ,disabled(config.disabled)

{
}


hsesslingen::opcua::ComponentConfig::~ComponentConfig()
{

}

const std::string &hsesslingen::opcua::ComponentConfig::getConfigLocation()
{
    return configLocation;
}

const std::string & hsesslingen::opcua::ComponentConfig::getName() const {
	return name;
}

const std::string & hsesslingen::opcua::ComponentConfig::getPathToBin() const {
    return pathToBin;
}

const bool hsesslingen::opcua::ComponentConfig::getDisabled() const
{
    return disabled;
}

void hsesslingen::opcua::ComponentConfig::setName(const std::string & name) {
	this->name = name;
}

void hsesslingen::opcua::ComponentConfig::setPathToBin(const std::string & pathToBin) {
    this->pathToBin = pathToBin;
}

void hsesslingen::opcua::ComponentConfig::setDisabled(bool value)
{
    disabled = value;
}

