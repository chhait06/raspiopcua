﻿#include "ServerConfig.h"

#include <fstream>
#include <iostream>

#include <UaLogger.h>

hsesslingen::opcua::ServerConfig::ServerConfig()
// : config(nullptr)
    : port(4840)
    , certificate(UA_BYTESTRING_NULL)
{

}

hsesslingen::opcua::ServerConfig::ServerConfig(const hsesslingen::opcua::ServiceConfig &serviceCfg)
    : port(serviceCfg.getPort())
    , certificate(loadServerCertificate(serviceCfg.getPathToCertificate()))
{

}

hsesslingen::opcua::ServerConfig::~ServerConfig()
{
    // if (config != nullptr) {
    //     UA_ServerConfig_delete(config);
    // }
    // config = nullptr;
    UA_ByteString_deleteMembers(&certificate);
}

UA_ServerConfig * hsesslingen::opcua::ServerConfig::getUaServerConfig()
{
    // if(certificate.data == UA_BYTESTRING_NULL.data)
    // {
    //     throw "empty certificate";
    // }

    UA_Log_Default = hsesslingen::opcua::UaLogger;
    auto config =UA_ServerConfig_new_minimal(port, &certificate);

    return config;
}

void hsesslingen::opcua::ServerConfig::setPortNumber(unsigned short portNumber) {
    port = portNumber;

}

void hsesslingen::opcua::ServerConfig::setServerCertificate(UA_ByteString && cert) {
    //FIXME: delete this->certificate before reassigning
    certificate = cert;
}

void hsesslingen::opcua::ServerConfig::deleteServerCertificate(UA_ByteString & cert) {
    UA_ByteString_deleteMembers(&cert);
    cert = UA_BYTESTRING_NULL;
}


UA_ByteString hsesslingen::opcua::ServerConfig::loadServerCertificate(const std::string & certPath)
{
    UA_ByteString cert = UA_BYTESTRING_NULL;

    std::ifstream certFile;
    certFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try {
        certFile.open(certPath, std::ios::in | std::ios::binary);

        if (!certFile.is_open())
        {
            //could not open
            throw 1;
        }
        certFile.seekg(0, certFile.end);
        cert.length = static_cast<size_t>( certFile.tellg() );
        certFile.seekg(0, certFile.end);

        if (cert.length != 0) {

            cert.data = static_cast<UA_Byte*>(UA_malloc(cert.length * sizeof(UA_Byte)));

            if (!cert.data) {
                throw "no memory left";
            }

            certFile.seekg(0, std::ios::beg);

            certFile.read((char *)cert.data, cert.length);



        }
        else{
            //Use null cert. For the UA_Server it is valid to use no cert at all
            cert = UA_BYTESTRING_NULL;
        }

        certFile.close();

    }
    catch (std::istream::failure ex) {
        UA_ByteString_deleteMembers(&cert);

        cert = UA_BYTESTRING_NULL;

    }
    catch (...) {
        certFile.close();
        UA_ByteString_deleteMembers(&cert);

        throw;
    }

    return cert;

}
