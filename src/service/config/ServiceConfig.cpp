#include "ServiceConfig.h"


hsesslingen::opcua::ServiceConfig::~ServiceConfig()
{
}

hsesslingen::opcua::ServiceConfig::ServiceConfig()
    : port(4840),
    pathToComponentFolder("."),
    pathToCertificate(),
    ignotedComponents()
{
}

hsesslingen::opcua::ServiceConfig::ServiceConfig(const hsesslingen::opcua::ServiceConfig &serviceConfig)
    :port{serviceConfig.port}
    ,pathToComponentFolder{serviceConfig.pathToComponentFolder}
    ,pathToCertificate{serviceConfig.pathToCertificate}
    ,ignotedComponents{serviceConfig.ignotedComponents}
{

}



unsigned short hsesslingen::opcua::ServiceConfig::getPort() const
{
	return this->port;
}

const std::string& hsesslingen::opcua::ServiceConfig::getPathToComponentFolder() const
{
	return this->pathToComponentFolder;
}

const std::string& hsesslingen::opcua::ServiceConfig::getPathToCertificate() const
{
    return this->pathToCertificate;
}

std::vector<std::string> &hsesslingen::opcua::ServiceConfig::getIgnoredComponents()
{
    return ignotedComponents;
}


void hsesslingen::opcua::ServiceConfig::setPort(short port) {
    this->port = port;
}

void hsesslingen::opcua::ServiceConfig::setPathToComponentFolder(std::string pathToComponentFolder)
{
	this->pathToComponentFolder = pathToComponentFolder;
}

void hsesslingen::opcua::ServiceConfig::setPathToCertificate(std::string pathToCertificate)
{
	this->pathToCertificate = pathToCertificate;
}
