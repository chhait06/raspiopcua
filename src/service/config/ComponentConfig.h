#ifndef COMPONENTCONFIG_H
#define COMPONENTCONFIG_H

#include <string>

namespace hsesslingen::opcua
{
class ComponentConfig
{
public:
    //Member
    ComponentConfig(std::string configPath);
    ComponentConfig(const ComponentConfig& config);

    const std::string& getConfigLocation();

    //Getter
    const std::string& getName() const;
    const std::string& getPathToBin() const;
    const bool getDisabled() const;

    //Setter
    void setName(const std::string &name);
    void setPathToBin(const std::string &pathToBin);
    void setDisabled(bool value);
    ~ComponentConfig();

private:
    std::string configLocation;

    std::string name;
    std::string pathToBin;
    bool disabled;
    //int pin;

};
}

#endif //COMPONENTCONFIG_H
