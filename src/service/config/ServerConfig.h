#ifndef SERVERCONFIG_H
#define SERVERCONFIG_H

#include <open62541.h>
#include <string>
#include <ServiceConfig.h>

namespace hsesslingen::opcua {

	class ServerConfig {

    public:
        //lädt das Zertifikat aus der angegebenen Datei und setzt es
        //0 bei erfolg, fehlercode bei fehler
        static UA_ByteString loadServerCertificate(const std::string & certPath);

        static void deleteServerCertificate(UA_ByteString & cert);

	public:
		ServerConfig();
        ServerConfig(const ServiceConfig & sr);
		~ServerConfig();

		UA_ServerConfig * getUaServerConfig();


	public:
		void setPortNumber(unsigned short portNumber);
        void setServerCertificate(UA_ByteString && cert);


	private:
        //UA_ServerConfig * config;

		unsigned short port;

		UA_ByteString certificate;


	};


}

#endif //SERVERCONFIG_H
