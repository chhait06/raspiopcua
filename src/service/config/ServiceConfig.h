#ifndef SERVICECONFIG_H
#define SERVICECONFIG_H

#include <string>
#include <vector>

namespace hsesslingen::opcua {

	class ServiceConfig {

    private:
        unsigned short port;
		std::string pathToComponentFolder;
		std::string pathToCertificate;
        std::vector<std::string> ignotedComponents;


	public:

		ServiceConfig();
        ServiceConfig(const hsesslingen::opcua::ServiceConfig &serviceConfig);
//        ServiceConfig(std::string serviceSection, short port, std::string pathToComponentFolder, std::string pathToCertificate) :
//             port(port), pathToComponentFolder(pathToComponentFolder), pathToCertificate(pathToCertificate) {
//		};
		~ServiceConfig();
        //ServerConfig &operator=(const ServerConfig &srvConfig);
        unsigned short getPort() const;
        const std::string& getPathToComponentFolder() const ;
        const std::string& getPathToCertificate() const ;
        std::vector<std::string>& getIgnoredComponents();

		void setServiceSection(std::string serviceSection);
		void setPort(short port);
		void setPathToComponentFolder(std::string PathToComponentFolder);
		void setPathToCertificate(std::string PathToCertificate);

	};
}

#endif //SERVICECONFIG_H
