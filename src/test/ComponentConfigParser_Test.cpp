#include "ComponentConfigParser_Test.h"
#include "ComponentConfigParser.h"
#include <iostream>
#include <exception>


int hsesslingen::test::ComponentConfigParserTest::run(std::string id, std::vector<std::string> & args)
{
	int ret = -1;
	if ("ComponentConfigParser" == id) {
		
		ret = ParseComponentConfig_Test(args);
		
	}

	return ret;
}

int hsesslingen::test::ComponentConfigParserTest::ParseComponentConfig_Test(std::vector<std::string> & arguments) {

	using namespace hsesslingen::opcua;

    //ComponentConfigParser ccp;
    ComponentConfig parsedComponentConfig{""};
	
    enum errorCode { good = 0x00, argumentSize = 0x01, parseError = 0x02, wrongName = 0x04, wrongPath = 0x08, wrongPin = 0x10};
    int returnValue = good;

    if (arguments.size() != 1) {
        returnValue |= argumentSize;
        std::cout << "to few or to less arguments, expect 4!" << "\n";
        return returnValue;
    }
	try {
        parsedComponentConfig = ComponentConfigParser::ParseConfig(arguments[0]);
	}
	catch(...) {
        returnValue |= parseError; // File konnte nicht geparst werden
        std::cout << u8"Could not find ini file to parse.\n";
        return returnValue;
	}
/*
	if (parsedComponentConfig.getName() != arguments[1]) {
        returnValue |= wrongName;
        std::cout << u8"Parsed name doesn't match with given argument name\n";
    }
	if (parsedComponentConfig.getPathToBin() != arguments[2]) {
        returnValue |= wrongPath;
        std::cout << u8"Parsed PathToBin doesn't match with given argument PathToBin\n";
    }
	if (parsedComponentConfig.getPin() != std::stoi(arguments[3])) {
        returnValue |= wrongPin;
        std::cout << u8"Parsed pin doesn't match with given argument pin\n";
    }
*/
    std::cout << "Name=" << parsedComponentConfig.getName() << " pathToBin=" << parsedComponentConfig.getPathToBin()  << std::endl;
    return returnValue;
}
