#https://cmake.org/cmake/help/v3.4/manual/cmake-properties.7.html#test-properties

#Demo ctest. Just to make sure that something is "testable"
#add_test(NAME DEMO_TEST COMMAND test_raspiopcua "DEMO" 0 42)
#set_tests_properties(DEMO_TEST PROPERTIES PASS_REGULAR_EXPRESSION "42")


#SignalSlot

add_test(NAME SignalSlot_tx_rx_empty COMMAND test_raspiopcua "SignalSlot" "test_tx_rx_empty" )
set_tests_properties(SignalSlot_tx_rx_empty PROPERTIES PASS_REGULAR_EXPRESSION "empty")

add_test(NAME SignalSlot_tx_rx_int COMMAND test_raspiopcua "SignalSlot" "test_tx_rx_int" 42)	
set_tests_properties(SignalSlot_tx_rx_int PROPERTIES PASS_REGULAR_EXPRESSION "42")

add_test(NAME SignalSlot_tx_rx_str_str COMMAND test_raspiopcua "SignalSlot" "test_tx_rx_str_str" "Text1" "Text2")	
set_tests_properties(SignalSlot_tx_rx_str_str PROPERTIES PASS_REGULAR_EXPRESSION "str1:Text1 str2:Text2")


###Test Parsing a single component config filey
set(TEST_COMPONENT_NAME "testComponent")
set(TEST_UACOMP_PATH "${TEST_COMPONENTS_BIN_DIR}/testComponent.uacomp")
configure_file(
    "${TEST_SRC_PATH}/iniFiles/components/generic_component.ini.in"
    "${TEST_COMPONENTS_CONFIG_DIR}/01-${TEST_COMPONENT_NAME}.ini"
    )
add_test(NAME ComponentConfigParser_01 COMMAND
    test_raspiopcua "ComponentConfigParser" "ComponentConfigParser" "${TEST_COMPONENTS_CONFIG_DIR}/01-${TEST_COMPONENT_NAME}.ini" )

set_tests_properties(ComponentConfigParser_01 PROPERTIES
    PASS_REGULAR_EXPRESSION "Name=${TEST_COMPONENT_NAME} pathToBin=${TEST_UACOMP_PATH}")

#Component

add_test(
    NAME Component_comp_load_dynlib
    COMMAND test_raspiopcua "Component" "test_comp_load_dynlib" "${TEST_COMPONENTS_CONFIG_DIR}/01-${TEST_COMPONENT_NAME}.ini"
    WORKING_DIRECTORY "${TEST_WORKING_ROOT}/"
    )
#set_tests_properties(SignalSlot_tx_rx_empty PROPERTIES PASS_REGULAR_EXPRESSION "")

###Test Parsing a single service config file
set(TEST_COMPONENTS_PATH "${TEST_COMPONENTS_CONFIG_DIR}")
set(TEST_CERT_PATH "cert/")
set(TEST_PORT_NR 4840)
configure_file(
    "${TEST_SRC_PATH}/iniFiles/service/generic_service.ini.in"
    "${TEST_WORKING_ROOT}/service_test.ini"
    )
add_test(NAME ServiceConfigParser_01 COMMAND
    test_raspiopcua "ServiceConfigParser" "ServiceConfigParser" "${TEST_WORKING_ROOT}/service_test.ini")

set_tests_properties(ServiceConfigParser_01 PROPERTIES
    PASS_REGULAR_EXPRESSION "compDir=${TEST_COMPONENTS_PATH} cert=${TEST_CERT_PATH} port=${TEST_PORT_NR}")

###ConfigLoader
set(TEST_COMPONENT_NAME "testComponent")
set(TEST_UACOMP_PATH "${TEST_COMPONENTS_BIN_DIR}/testComponent.uacomp")
configure_file(
    "${TEST_SRC_PATH}/iniFiles/components/generic_component.ini.in"
    "${TEST_COMPONENTS_CONFIG_DIR}/02-${TEST_COMPONENT_NAME}.ini"
    )

add_test(NAME ConfigLoader_Emit_ComponentConfig COMMAND
    test_raspiopcua "ConfigLoader" "ConfigLoader_Emit_ComponentConfig" "${TEST_COMPONENTS_CONFIG_DIR}/")

#add all Component Config files as dependecy. Each file should be parsed to pass the test
file(GLOB configFiles "${TEST_COMPONENTS_CONFIG_DIR}/*.ini")
foreach(file ${configFiles})
    string(REGEX MATCH "(.*\/)([0-9]+)\-(.+)(\.ini)" REXFILE "${file}")
    set_tests_properties(ConfigLoader_Emit_ComponentConfig PROPERTIES
        PASS_REGULAR_EXPRESSION "${CMAKE_MATCH_3}\n")
endforeach()


###ComponentManager
add_test(
    NAME ComponentManager_Basic_Test
    COMMAND test_raspiopcua "ComponentManager" "ComponentManager_Basic_Test" "${TEST_COMPONENTS_CONFIG_DIR}/"
    WORKING_DIRECTORY "${TEST_WORKING_ROOT}/"
    )

#add all Component Config files as dependecy. Each file should be parsed to pass the test
file(GLOB configFiles "${TEST_COMPONENTS_CONFIG_DIR}/*.ini")
foreach(file ${configFiles})
    string(REGEX MATCH "(.*\/)([0-9]+)\-(.+)(\.ini)" REXFILE "${file}")
    set_tests_properties(ComponentManager_Basic_Test PROPERTIES
        PASS_REGULAR_EXPRESSION "${CMAKE_MATCH_3}\n")
endforeach()
