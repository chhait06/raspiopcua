#ifndef SIGNALSLOT_TEST_H
#define SIGNALSLOT_TEST_H

#include <string>
#include <vector>

namespace hsesslingen::test {

	class SignalSlot_Test {

	public:
		int run(std::string id, std::vector<std::string> & args);


		int test_tx_rx_empty(std::vector<std::string> args);
		int test_tx_rx_int(std::vector<std::string> args);
		int test_tx_rx_str_str(std::vector<std::string> args);
		//int test_tx_rx_order_void(std::vector<std::string> args);

	};
}
#endif //SIGNALSLOT_TEST_H