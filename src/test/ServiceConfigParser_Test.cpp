#include "ServiceConfigParser_Test.h"
#include "ServiceConfigParser.h"
#include <iostream>
#include <exception>

int hsesslingen::test::ServiceConfigParserTest::run(std::string id, std::vector<std::string> & args)
{
    int ret = -1;
    if ("ServiceConfigParser" == id) {

        ret = ParseServiceConfig_Test(args);

    }

    return ret;
}

int hsesslingen::test::ServiceConfigParserTest::ParseServiceConfig_Test(std::vector<std::string> & arguments) {
    using namespace hsesslingen::opcua;

    ServiceConfig parsedServiceConfig;

    enum errorCode { good = 0x00, argumentSize = 0x01, parseError = 0x02, wrongPort = 0x04, wrongPathToComp = 0x08, wrongPathToCertificate = 0x10};
    int returnValue = good;

    if (arguments.size() != 1) {
        returnValue |= argumentSize;
        std::cout << "to few or to less arguments, expect 4!" << "\n";
        return returnValue;
    }
    try {
        parsedServiceConfig = ServiceConfigParser::ParseConfig(arguments[0]);
    }
    catch(...) {
        returnValue |= parseError; // File konnte nicht geparst werden
        std::cout << u8"Could not find ini file to parse.\n";
        return returnValue;
    }
/*
    if (parsedServiceConfig.getPort() != arguments[1]) {
        returnValue |= wrongPort;
        std::cout << u8"Parsed name doesn't match with given argument name\n";
    }
    if (parsedServiceConfig.getPathToComponentFolder() != arguments[2]) {
        returnValue |= wrongPathToComp;
        std::cout << u8"Parsed PathtoComp doesn't match with given argument PathtoComp\n";
    }
    if (parsedServiceConfig.getPathToCertificate() != arguments[3]) {
        returnValue |= wrongPathToCertificate;
        std::cout << u8"Parsed PathToCertificate doesn't match with given argument PathToCertificate\n";
    }
*/
    std::cout << "compDir=" << parsedServiceConfig.getPathToComponentFolder() << " cert=" << parsedServiceConfig.getPathToCertificate() << " port=" << parsedServiceConfig.getPort() << std::endl;
    return returnValue;
}
