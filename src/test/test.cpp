#include <clocale>
#include <string>
#include <iostream>


#include "SignalSlot_Test.h"
#include "ComponentConfigParser_Test.h"
#include "ServiceConfigParser_Test.h"
#include "ConfigLoader_Test.h"
#include "Component_Test.h"
#include "ComponentManager_Test.h"
//#include "newTest.h"

int main(int argc, char** argv )
{
	int returnCode = 0;

	if (argc <= 2) {
		//No test is selected
		std::cout << "arg 1: unit name; arg 2: test id; (arg 3,4,..: test properties)" << std::endl;
		return 1;
	}

	std::string unitName(argv[1]);
	std::string testId(argv[2]);

	int propertyCount = argc - 3;

	if (propertyCount < 0) propertyCount = 0;	//no test arguments

	std::vector<std::string> arguments(propertyCount);

	for (int i = 3; i < argc; i++) {
		arguments[i - 3] = argv[i];
	}
	
	if ( "DEMO" == unitName ) {

		if (arguments.size() > 0)
		{
			std::cout << arguments[0] << std::endl;
		}
		returnCode = 0;
	}
	else if ( "SignalSlot" == unitName ) {
		
		hsesslingen::test::SignalSlot_Test sst;
		
		returnCode = sst.run(testId, arguments);
	}

    else if ( "ComponentConfigParser" == unitName) {
		hsesslingen::test::ComponentConfigParserTest ccpt;

		returnCode = ccpt.run(testId, arguments);
	}	
    else if ( "ServiceConfigParser" == unitName) {
        hsesslingen::test::ServiceConfigParserTest scpt;

        returnCode = scpt.run(testId, arguments);
    }
    else if ( "ConfigLoader" == unitName) {
        hsesslingen::test::ConfigLoader_Test clt;

        returnCode = clt.run(testId, arguments);
    }else if ( "ComponentManager" == unitName) {
        hsesslingen::test::ComponentManager_Test cmt;

        returnCode = cmt.run(testId, arguments);
    }else if ( "Component" == unitName) {
        hsesslingen::test::Component_Test comp;

        returnCode = comp.run(testId, arguments);
    }
    return returnCode;
}
