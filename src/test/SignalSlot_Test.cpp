#include "SignalSlot_Test.h"

#include <iostream>

#include <SignalSlot.h>

using hsesslingen::util::Signal;
using hsesslingen::util::Slot;


int hsesslingen::test::SignalSlot_Test::run(std::string id, std::vector<std::string> & args)
{
	int returnValue = 0; 
	if ("test_tx_rx_empty" == id)
	{
		returnValue = test_tx_rx_empty(args);
	}
	else if ("test_tx_rx_int" == id)
	{
		returnValue = test_tx_rx_int(args);
	}
	else if ("test_tx_rx_str_str" == id)
	{
		returnValue = test_tx_rx_str_str(args);
	}
	/*else if ("test_tx_rx_order_void" == id)
	{
		returnValue = test_tx_rx_order_void(args);
	}*/

	return returnValue;
}

int hsesslingen::test::SignalSlot_Test::test_tx_rx_empty(std::vector<std::string> args)
{
	int ret = -1;

	Signal<> signal;
	Slot<> slot;
	
	slot.setReceiveHandle([&ret]() 
	{
		std::cout << "empty" << std::endl;
		ret = 0;
	});

	signal.connect(slot);

	signal.emit();

	return ret;
}

int hsesslingen::test::SignalSlot_Test::test_tx_rx_int(std::vector<std::string> args)
{
	int ret = -1;

	int arg = std::stoi(args[0]);

	Signal<int> signal;
	Slot<int> slot;

	slot.setReceiveHandle([&ret, &arg ](int& data)
	{
		std::cout << data << std::endl;
		
		if (data == arg) 
		{
			ret = 0;
		}
	});

	signal.connect(slot);

	signal.emit(arg);

	return ret;
}

int hsesslingen::test::SignalSlot_Test::test_tx_rx_str_str(std::vector<std::string> args)
{
	int ret = -1;
	if (args.size() != 2) throw "wrong argument count";

	Signal<std::string, std::string> signal;
	Slot<std::string, std::string> slot;

	slot.setReceiveHandle([&ret](std::string str1, std::string str2 )
	{
		std::cout << "str1:" << str1 <<" str2:"<< str2 << std::endl;
		ret = 0;
	});

	signal.connect(slot);

	signal.emit(args[0], args[1]);

	return ret;
}

/*int hsesslingen::test::SignalSlot_Test::test_tx_rx_order_void(std::vector<std::string> args)
{
	int ret = -1;

	Signal<> signal;
	Slot<> slot;

	slot.setReceiveHandle([&ret](auto& sender)
	{
		std::cout << "empty" << std::endl;
		ret = 0;
	});

	signal.connect(slot);

	signal.emit();

	return ret;
}*/