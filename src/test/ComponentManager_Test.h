#ifndef COMPONENTMANAGER_TEST_H
#define COMPONENTMANAGER_TEST_H

#include <string>
#include <vector>


namespace hsesslingen::test
{
    class ComponentManager_Test
    {
    public:
        int run(std::string & id, std::vector<std::string> & args);
    private:
        int LoadComponents_Test(std::vector<std::string> & arguments);
    };
}
#endif //COMPONENTMANAGER_TEST_H
