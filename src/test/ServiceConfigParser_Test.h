#ifndef SERVICECONFIGPARSER_TEST_H
#define SERVICECONFIGPARSER_TEST_H
#include <string>
#include <vector>
namespace hsesslingen::test {

	class ServiceConfigParserTest {
	public:
        int run(std::string id, std::vector<std::string> & args);
        int ParseServiceConfig_Test(std::vector<std::string> & arguments);
	};
}
#endif // SERVICECONFIGPARSER_TEST_H
