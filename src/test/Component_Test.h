#ifndef COMPONENT_TEST_H
#define COMPONENT_TEST_H

#include <string>
#include <vector>

namespace hsesslingen::test{
    class Component_Test
    {
    public:
        int run(std::string id, std::vector<std::string> & args);

    private:
        int test_comp_load_dynlib(std::vector<std::string> args);
    };
}
#endif // COMPONENT_TEST_H
