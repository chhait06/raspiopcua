#include "ComponentManager_Test.h"

#include <iostream>

#include <ConfigLoader.h>
#include <ComponentManager.h>


int hsesslingen::test::ComponentManager_Test::run(std::string &id, std::vector<std::string> &args)
{
    int ret = -1;
    if ("ComponentManager_Basic_Test" == id) {

        ret = LoadComponents_Test(args);
    }
    return ret;
}


int hsesslingen::test::ComponentManager_Test::LoadComponents_Test(std::vector<std::string> &arguments) {
    int ret = 0;
    hsesslingen::opcua::ConfigLoader cl;
    hsesslingen::opcua::ComponentManager cm(nullptr);

    cl.ComponentConfigParsedSignal.connect(cm.ParsedComponentConfigSlot);

    cl.LoadComponents(arguments[0]);


    for(const auto& c : cm.getComponentNames())
    {
        std::cout << c << '\n';
    }

    return ret;
}

