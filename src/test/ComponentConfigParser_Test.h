#ifndef COMPONENTCONFIGPARSER_TEST
#define COMPONENTCONFIGPARSER_TEST
#include <string>
#include <vector>

namespace hsesslingen::test {

	class ComponentConfigParserTest {
	public:
		int run(std::string id, std::vector<std::string> & args);

		int ParseComponentConfig_Test(std::vector<std::string> & arguments);
	};
}
#endif // COMPONENTCONFIGPARSER_TEST