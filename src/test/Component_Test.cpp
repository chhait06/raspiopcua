#include "Component_Test.h"

#include <Component.h>
#include <ComponentConfigParser.h>

namespace ho = hsesslingen::opcua;

int hsesslingen::test::Component_Test::run(std::string id, std::vector<std::string> &args)
{
    int returnValue = 0;
    if ("test_comp_load_dynlib" == id)
    {
        returnValue = test_comp_load_dynlib(args);
    }
    return returnValue;
}


int hsesslingen::test::Component_Test::test_comp_load_dynlib(std::vector<std::string> args)
{
    int ret = 0;
    auto cfg = ho::ComponentConfigParser::ParseConfig(args[0]);

    try
    {
        ho::Component comp(cfg);
        auto api =comp.api();
    }
    catch(...)
    {
        ret = -1;
    }
    return ret;
}
