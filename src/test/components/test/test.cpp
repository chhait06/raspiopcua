#include "test.h"

#include <errno.h> // errno, EINTR
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
//#include <Log.h>


//GetComponentHandleFunction getComponentHandle = getTestComponentHandle;

UA_Int32 randInt = 0;

struct ComponentApi getComponentApiHandle() {
	return getTestComponenApi();
}

struct ComponentApi getTestComponenApi()
{
//	LOG(Log::Level::Informational, L_UNIT("testCompunent"), L_MESSAGE("Create component instace"));
	struct ComponentApi cmp = { 0 };
	
	cmp.initFn = init;
	cmp.nodesFn = addNotes;

	return cmp;
}

//int(*InitComponentFunction)(ComponentData *);
int init(struct ComponentHandle * comp)
{
    //LOG(Log::Level::Informational, L_UNIT("testCompunent"), L_MESSAGE("init component instace"));
	return 0;
}

//int(*AddComponentNodesFunction)(ComponentData *);
int addNotes(struct ComponentHandle * comp)
{	

//	LOG(Log::Level::Informational, L_UNIT("testCompunent"), L_MESSAGE("add Nodes to the server"));
	return addTestNodes(comp->server);
}


static UA_StatusCode getTestDaten(UA_Server *server,
	const UA_NodeId *sessionId, void *sessionContext,
	const UA_NodeId *nodeId, void *nodeContext,
	UA_Boolean sourceTimeStamp,
	const UA_NumericRange *range, UA_DataValue *value) {
	if (range) {
		value->hasStatus = true;
		value->status = UA_STATUSCODE_BADINDEXRANGEINVALID;
		return UA_STATUSCODE_GOOD;
	}
	UA_StatusCode ret;
	//UA_Int32 randInt = (int32_t)UA_DateTime_now(); //pseudo zufall
	randInt++;

	//Hier werden Datenwerte gelesen (Datei/GPIO_PIN/Netzwerk)
        //Lokale Methode welche die Datenabruft, diese Methode ist für die komponente Eindeutig
		//int GiveData(spome Parameters)
		//int myData = GiveData(...)
		//fscanf(tempFile, "%d", &temp)
        //Kurze Delay Time, WatchDog zur überprüfung evtl.

	//writeNoteValue(1337, temp)



	//
	//Hier wird der Node der Datenwert zugewiesen
	ret = UA_Variant_setScalarCopy(&value->value, &randInt, &UA_TYPES[UA_TYPES_INT32]);
	value->hasValue = true;
	if (sourceTimeStamp) {
		value->hasSourceTimestamp = true;
		value->sourceTimestamp = UA_DateTime_now();
	}

	if (randInt % 10 == 0) {
		ret = UA_STATUSCODE_BADNODATAAVAILABLE;
	}
	
	return ret;// UA_STATUSCODE_BADNODATAAVAILABLE; //ret; // UA_STATUSCODE_GOOD;
}

void  onReadZufallsWert(UA_Server *server, const UA_NodeId *sessionId,
	void *sessionContext, const UA_NodeId *nodeid,
	void *nodeContext, const UA_NumericRange *range,
	const UA_DataValue *value) {

	UA_Variant val;
    UA_String myStr = UA_STRING(const_cast<char*>(u8"LECK MICH"));
	UA_Variant_setScalarCopy(&val, &myStr, &UA_TYPES[UA_TYPES_STRING]);

    UA_NodeId currentNodeId = UA_NODEID_STRING(1, const_cast<char*>(u8"zufallswert"));

	UA_Server_writeValue(server, currentNodeId, val);

}

int nodeValue1337 = 1337;

int addTestNodes(UA_Server* server) {
	UA_StatusCode ret;




	//test node
	UA_DataSource eigeneDatenQuelle;
	eigeneDatenQuelle.read = getTestDaten;
	eigeneDatenQuelle.write = NULL;
	UA_VariableAttributes v_attr = UA_VariableAttributes_default;
    v_attr.description = UA_LOCALIZEDTEXT(const_cast<char*>(u8"en-US"), const_cast<char*>(u8"zufallszahl"));
    v_attr.displayName = UA_LOCALIZEDTEXT(const_cast<char*>(u8"en-US"), const_cast<char*>(u8"zufallszahl"));
	v_attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
	v_attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    const UA_QualifiedName dateName = UA_QUALIFIEDNAME(1, const_cast<char*>(u8"zufallszahlBN"));

	UA_Int32 val = 0;

	//UA_Variant_setScalarCopy(&v_attr.value, &val , &UA_TYPES[UA_TYPES_INT32]);

	ret = UA_Server_addDataSourceVariableNode(
		server,
		UA_NODEID_NUMERIC(1, 1337),//	UA_NODEID_NULL, 
		UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
		UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
		dateName,
		UA_NODEID_NULL,//UA_TYPES[UA_TYPES_INT32].typeId,
		v_attr,
		eigeneDatenQuelle,
		&nodeValue1337,//NULL,
		NULL);
	// */		


	return ret;
}
