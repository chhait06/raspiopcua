#ifndef EIGENNODE_H
#define EIGENNODE_H

#include <open62541.h>

#include <component_base.h>
#include <component_handle.h>


struct ComponentApi getTestComponenApi();

//extern "C" GetComponentHandleFunction getComponentHandle = getTestComponentHandle;

int init(struct ComponentHandle * comp);

int addNotes(struct ComponentHandle * comp);

int addTestNodes(UA_Server* server);

#endif // EIGENNODE_H