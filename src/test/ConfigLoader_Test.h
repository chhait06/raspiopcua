#ifndef CONFIGLOADER_TEST_H
#define CONFIGLOADER_TEST_H

#include <string>
#include <vector>

namespace hsesslingen::test {
  class ConfigLoader_Test {
    public:
      ConfigLoader_Test();
      int run(std::string id, std::vector<std::string> & args);
      int ConfigLoader_Emit_ComponentConfig(std::vector<std::string> & args);
   };
}


#endif // CONFIGLOADER_TEST_H
