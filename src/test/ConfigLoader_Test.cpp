#include "ConfigLoader_Test.h"
#include "ConfigLoader.h"
#include "SignalSlot.h"
#include <iostream>

hsesslingen::test::ConfigLoader_Test::ConfigLoader_Test()
{

}

int hsesslingen::test::ConfigLoader_Test::run(std::string id, std::vector<std::string> & args) {
    int ret = -1;
    if ("ConfigLoader_Emit_ComponentConfig" == id) {

        ret = ConfigLoader_Emit_ComponentConfig(args);

    }

    return ret;
}
int hsesslingen::test::ConfigLoader_Test::ConfigLoader_Emit_ComponentConfig(std::vector<std::string> & args) {
    int ret = 0;

    if(args.size() != 1) {
        std::cout << "expected one inputer paramter, got:" << args.size() << "\n";
    }


    hsesslingen::opcua::ConfigLoader cl;
    hsesslingen::util::Slot<hsesslingen::opcua::ComponentConfig> slot;


    slot.setReceiveHandle([ ](hsesslingen::opcua::ComponentConfig& data)
    {
        std::cout << data.getName() << std::endl;
    });

    cl.ComponentConfigParsedSignal.connect(slot);

    cl.LoadComponents(args[0]);

    return 0;
}
