#ifndef MEDIACOMPONENTBASE_H
#define MEDIACOMPONENTBASE_H

#include <open62541.h>

#include <component_base.h>
#include <component_handle.h>


//extern "C" struct ComponentApi getComponentApiHandle();

int initFn(struct ComponentHandle * comp);

int notesFn(struct ComponentHandle * comp);

int startFn(struct ComponentHandle * comp);

int iterFn(struct ComponentHandle * comp);

int stopFn(struct ComponentHandle * comp);

int deleteFn(struct ComponentHandle * comp);

#endif // MEDIACOMPONENTBASE_H
