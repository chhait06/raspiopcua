#include "Component_Base.h"

#include <Log.h>

//#define TO_COMPONENT_OBJECT( comp ) ( *static_cast<COMPONENT_TYPE*>( comp->context ) )

struct ComponentApi getComponentApiHandle() {
    struct ComponentApi cmp = { 0 };

    cmp.initFn = initFn;
    cmp.nodesFn = notesFn;
    cmp.startFn = startFn;
    cmp.iterateFn = iterFn;
    cmp.stopFn = stopFn;
    cmp.deleteFn = deleteFn;

    return cmp;
}


int initFn(struct ComponentHandle * comp)
{
    LOG(Log::Level::Informational, L_UNIT("COMPONENT_NAME"), L_MESSAGE("init component instace"));

    //comp->context = new context...
    return 0;
}

int notesFn(struct ComponentHandle * comp)
{
    int ret = 0;
    if(comp->context){


    }
    return ret;
}

int iterFn(ComponentHandle *comp)
{
    int ret = 0;
    if(comp->context){


    }
    return ret;
}

int deleteFn(ComponentHandle *comp)
{
    if(comp->context){

        //delete context;

    }

    return 0;
}


int startFn(ComponentHandle *comp)
{
    LOG(Log::Level::Informational, L_UNIT("COMPONENT_NAME"), L_MESSAGE("starting..."));
    int ret = 0;
    if(comp->context){


    }
    return ret;
}

int stopFn(ComponentHandle *comp)
{
    int ret = 0;
    if(comp->context){


    }
    return ret;
}
