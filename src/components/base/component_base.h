
#ifndef COMPONENT_BASE_H
#define COMPONENT_BASE_H

#include <component_export.h>
#include <component_api.h>

//extern "C" COMPONENT_EXPORT int initComponent(UA_Server* server);

extern "C" {

	COMPONENT_EXPORT struct ComponentApi getComponentApiHandle();

}

#endif //COMPONENT_BASE_H