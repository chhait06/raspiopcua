#ifndef COMPONENT_API_H
#define COMPONENT_API_H

extern "C" {

	static const char* API_HANDLE_SYMBOL_NAME = "getComponentApiHandle";

	struct ComponentHandle;

	typedef int(*InitComponentFn)		(struct ComponentHandle *);
	typedef int(*AddComponentNodesFn)	(struct ComponentHandle *);
	typedef int(*StartComponentFn)		(struct ComponentHandle *);
	typedef int(*IterateComponentFn)	(struct ComponentHandle *);
	typedef int(*StopComponentFn)		(struct ComponentHandle *);
	typedef int(*DeleteComponentFn)		(struct ComponentHandle *);

	// 	typedef struct ComponentApi		ComponentApi;
	struct ComponentApi {

		//Komponente Initialisieren
		InitComponentFn		initFn;

        //Nodes hinzufügen
		AddComponentNodesFn	nodesFn;

		//Loop Funktionen 
		StartComponentFn	startFn;
		IterateComponentFn	iterateFn;
		StopComponentFn		stopFn;

        //Komponente löschen
		DeleteComponentFn	deleteFn;
	};

	typedef struct ComponentApi(*GetComponentApiHandleFn)();	
}

#endif // COMPONENT_API_H
