#ifndef COMPONENT_EXPORT_H
#define COMPONENT_EXPORT_H

#if defined(_WIN32)
# ifndef IMPORT_COMPONENT // export dll
#  ifdef __GNUC__
#   define COMPONENT_EXPORT __attribute__ ((dllexport))
#  else
#   define COMPONENT_EXPORT __declspec(dllexport)
#  endif
# else // import dll -> add to cmake target_compile_definitions( <projectname> PRIVATE IMPORT_COMPONENT)
#  ifdef __GNUC__
#   define COMPONENT_EXPORT __attribute__ ((dllimport))
#  else
#   define COMPONENT_EXPORT __declspec(dllimport)
#  endif
# endif
#else // non win32 
# if __GNUC__ || __clang__
#  define COMPONENT_EXPORT __attribute__ ((visibility ("default")))
# endif
#endif

#ifndef COMPONENT_EXPORT
# define COMPONENT_EXPORT // fallback to default 
#endif


//#if defined(_WIN32)
//# ifdef EXPORTING_COMPONENT // export dll
//#  ifdef __GNUC__
//#   define COMPONENT_EXPORT __attribute__ ((dllexport))
//#  else
//#   define COMPONENT_EXPORT __declspec(dllexport)
//#  endif
//# else // import dll
//#  ifdef __GNUC__
//#   define COMPONENT_EXPORT __attribute__ ((dllimport))
//#  else
//#   define COMPONENT_EXPORT __declspec(dllimport)
//#  endif
//# endif
//#else // non win32 
//# if __GNUC__ || __clang__
//#  define COMPONENT_EXPORT __attribute__ ((visibility ("default")))
//# endif
//#endif
//#ifndef COMPONENT_EXPORT
//# define COMPONENT_EXPORT // fallback to default 
//#endif

#endif //COMPONENT_EXPORT_H
