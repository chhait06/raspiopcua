#ifndef COMPONENT_HANDLE_H
#define COMPONENT_HANDLE_H

#include <open62541.h>

#include <component_api.h>

extern "C" {

	struct ComponentHandle {
		//Server der die Plugin-Instanz nutzt
		UA_Server *			server;
		
		//API-Funktionen des Plugins
        struct ComponentApi  api;

        //Datenkontext für das speichern von Daten, die von einer Instanz des Plugins genutzt werden.
		void *				context;

        //Pfad zur Konfigurationsdatei dieser Komponente
        const char * config_file;
	} ;

}

#endif //COMPONENT_HANDLE_H
