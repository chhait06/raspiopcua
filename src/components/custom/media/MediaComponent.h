#ifndef MEDIACOMPONENT_H
#define MEDIACOMPONENT_H

#include <open62541.h>

#include <component_handle.h>

#include <systemd/sd-bus.h>

#include <vector>
#include <map>
#include <string>
#include <string_view>


#include <future>


class MediaComponent{
    using MediaPlayerIdMap = std::map<const std::string, const std::string, std::less<>>;
    using Context = std::pair<const MediaComponent*, std::string>;
public:
    MediaComponent( UA_Server * server_);

public:
    int AddNotes();
    int Iterate();
    int Start();
    int Stop();


private:
    int addBaseMediaNodes();

    //false -> nothing/allready existing, true -> player added
    bool addMediaPlayer(const std::string_view & dbusMediaPlayerName);

    void addMediaPlayerNode(const std::string_view & dbusMediaPlayerName);


    void removeMediaPlayer(const std::string_view & dbusMediaPlayerName);

    bool removeMediaPlayerNode(const std::string_view & dbusMediaPlayerName);


    void updateMediaPlayers();

    bool memorizeMediaPlayerName(const std::string_view & mediaPlayerName, MediaPlayerIdMap::iterator* insertedElement_out = nullptr);



    void writeNodeStr(const UA_NodeId& nId, const std::string_view& text);
    void writeCoverUrlNode(const std::string_view& playerDbusId, const std::string_view& coverUrl);
    void startCoverImageUpdate(const std::string_view& playerDbusId, const std::string_view& coverUrl);
    void processCoverImageUpdate(const std::string_view& playerDbusId);

    void writeCoverNode(UA_NodeId nId, const std::vector<char> && imageData);

    bool needCoverNodeUpdate(const std::string_view& playerDbusId, const std::string_view newCoverUrl);

    std::vector<std::string_view> getPlayerKeys() const;

    Context * makeContext(std::string_view content );
    void deleteContext(Context* context );


private:
    UA_Server* const server;
    UA_NodeId mediaControlNode;
    sd_bus * bus;
    int namespaceIndex;
    MediaPlayerIdMap mediaPlayerIDs; // dbusId, name

    std::map< const std::string_view,
        std::pair<
            const std::string,
            std::future<std::vector<char>>>> coverLaoders;
};

/*
template<typename MediaPlayerFound>
void findMediaPlayers(sd_bus* bus, MediaPlayerFound callback);

const std::string baseMediaPlayerName{u8"org.mpris.MediaPlayer2."};

template <typename ArtistCallback, typename TitleCallback>
void grepMetadataDbus(sd_bus* bus, const std::string medisPlayerDbusId, ArtistCallback artistFn, TitleCallback titleFn);

bool isMediaPlayer(const char* mediaPlayerName);

int connectToUserSession( sd_bus* & bus);

std::string dbusName2CleanName(const std::string & dbusMediaPlayerName);

std::string name2DbusName(const std::string & mediaPlayerName);
*/

#endif // MEDIACOMPONENT_H
