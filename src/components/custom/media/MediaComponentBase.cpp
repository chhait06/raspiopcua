#include "MediaComponentBase.h"
#include "MediaComponent.h"

#include <Log.h>

#define MC( comp ) ( *static_cast<MediaComponent*>( comp->context ) )

struct ComponentApi getComponentApiHandle() {
    return getMediaComponenApi();
}

struct ComponentApi getMediaComponenApi()
{
    //LOG(Log::Level::Informational, L_UNIT("mediaComponent"), L_MESSAGE("Create component instace"));
	struct ComponentApi cmp = { 0 };
	
    cmp.initFn = initFn;
    cmp.nodesFn = notesFn;
    cmp.startFn = startFn;
    cmp.iterateFn = iterFn;
    cmp.stopFn = stopFn;
    cmp.deleteFn = deleteFn;

	return cmp;
}

//int(*InitComponentFunction)(ComponentData *);
int initFn(struct ComponentHandle * comp)
{
    LOG(Log::Level::Informational, L_UNIT("mediaComponent"), L_MESSAGE("init component instace"));

    MediaComponent* mc = new MediaComponent(comp->server);

    comp->context = mc;

	return 0;
}

int notesFn(struct ComponentHandle * comp)
{	
    LOG(Log::Level::Informational, L_UNIT("mediaComponent"), L_MESSAGE("add Nodes to the server"));

    int ret = 0;
    if(comp->context){
        ret = MC(comp).AddNotes();
    }
    return ret;
}

int iterFn(ComponentHandle *comp)
{
    int ret = 0;
    if(comp->context){
        ret = MC(comp).Iterate();
    }
    return ret;
}

int deleteFn(ComponentHandle *comp)
{
    if(comp->context){
        delete &MC(comp);
    }

    return 0;
}


int startFn(ComponentHandle *comp)
{
    LOG(Log::Level::Informational, L_UNIT("mediaComponent"), L_MESSAGE("starting..."));
    int ret = 0;
    if(comp->context){
        ret = MC(comp).Start();
    }
    return ret;
}

int stopFn(ComponentHandle *comp)
{
    int ret = 0;
    if(comp->context){
        ret = MC(comp).Stop();
    }
    return ret;
}
