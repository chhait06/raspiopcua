﻿#include "MediaComponent.h"
#include "DbusMediaUtil.h"

//#include <systemd/sd-bus-protocol.h>
#include <Log.h>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <functional>
#include <cerrno>
#include <filesystem>
#include <fstream>
#include <chrono>
#include <iterator>
#include <cstdio>
#include <unistd.h>

#define NODEID_PLAYER_A(dbusID)   (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))) )))
#define NODEID_ARTIST_A(dbusID)   (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Artist") )))
#define NODEID_TITLE_A(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Title") )))
#define NODEID_COVER_A(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Cover") )))
#define NODEID_COVERURL_A(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".CoverUrl") )))

#define NODEID_NEXT_A(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Next") )))
#define NODEID_PREVIOUS_A(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Previous") )))
#define NODEID_PLAYPAUSE_A(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".PlayPause") )))


#define NODEID_PLAYER(dbusID)   (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))) )))
#define NODEID_ARTIST(dbusID)   (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Artist") )))
#define NODEID_TITLE(dbusID)    (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Title") )))
#define NODEID_COVER(dbusID)    (UA_NODEID_STRING_ALLOC(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Cover") )))
#define NODEID_COVERURL(dbusID)    (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".CoverUrl") )))

#define NODEID_NEXT(dbusID)    (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Next") )))
#define NODEID_PREVIOUS(dbusID)    (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".Previous") )))
#define NODEID_PLAYPAUSE(dbusID)    (UA_NODEID_STRING(namespaceIndex, C_STR( (mediaPlayerIDs.at(C_STR(dbusID))+".PlayPause") )))

#define CONTEXT_PTR( ptr ) (static_cast<MediaComponent::Context*>( ptr ))

using DMU = DbusMediaUtil ;

MediaComponent::MediaComponent(UA_Server* server_)
    : server(server_)
    , mediaControlNode(UA_NODEID_NULL)
    , bus (nullptr)
{ }

int MediaComponent::AddNotes()
{
    int ret = 0;

    namespaceIndex = UA_Server_addNamespace(server, U8("MediaPlayerControll"));

    ret = addBaseMediaNodes();

    return ret;
}

int MediaComponent::Start()
{
    if(DbusMediaUtil::connectToUserSession(bus) < 0)
    {
        LOG(Log::Level::Informational,
            L_UNIT("mediaComponent"),
            L_MESSAGE("could not connect to dbus user session"));
    }

    return 0;
}

int MediaComponent::Iterate()
{


    int ret = 0;
    if(sd_bus_is_ready(bus) < 0)
    {
        return ENXIO;
    }

    updateMediaPlayers();

    for(const auto & playerDbusIdPair : mediaPlayerIDs)
    {
        const std::string & playerDbusId = playerDbusIdPair.first;

        DMU::grepMetadataDbus(bus, playerDbusId, [this, &playerDbusId](const  auto& artists)
        {
            std::string artistsCompined;
            for(auto it = artists.begin(); it != artists.end(); it++)
            {
                const auto& a = *it;

                artistsCompined += a;
                if(it+1 != artists.end())
                {
                    artistsCompined += ",";
                }
            }
            //std::cout << "call artist\n";

            writeNodeStr(NODEID_ARTIST(playerDbusId), artistsCompined);


        },
        [this, &playerDbusId](const  auto& title)
        {
            writeNodeStr(NODEID_TITLE(playerDbusId), title);
            //std::cout << "call title\n";
        },
        [this, &playerDbusId](const auto& coverUrl)
        {
            if( needCoverNodeUpdate(playerDbusId, coverUrl)){
                writeCoverUrlNode(playerDbusId, coverUrl);
                startCoverImageUpdate(playerDbusId,coverUrl);
            }
        });

        processCoverImageUpdate(playerDbusId);
    }
    return ret;
}

int MediaComponent::Stop()
{
    if(bus)
    {
        sd_bus_unref(bus);
        bus=nullptr;
    }
    return 0;
}

int MediaComponent::addBaseMediaNodes() {

    int ret = 0;

    //Add Base Media Node
    UA_ObjectAttributes mediaControl_attr = UA_ObjectAttributes_default;
    mediaControl_attr.displayName = UA_LOCALIZEDTEXT(U8("en-EN"), U8("MediaControl"));
    mediaControl_attr.description = UA_LOCALIZEDTEXT(U8("en-EN"), U8("Shows all media players supporting this interface"));


    ret = UA_Server_addObjectNode(server,UA_NODEID_NUMERIC(namespaceIndex, 1), //UA_NODEID_NULL,
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                  UA_QUALIFIEDNAME(1, const_cast<char*>(u8"MediaControl")),
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_FOLDERTYPE),
                                  mediaControl_attr,
                                  NULL, &mediaControlNode);
    return ret;
}

void MediaComponent::writeNodeStr(const UA_NodeId& nId, const std::string_view& text)
{
    UA_String str = UA_STRING(C_STR(text));
    UA_Variant value;

    UA_Variant_setScalar(&value, &str, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_writeValue(server, nId, value);
}

std::vector< char > loadCoverFromJpgUrl(const std::string_view& jpgUrl){

    namespace fs = std::filesystem;

    std::vector<char > imageBuffer;

    std::FILE* tmpf = std::tmpfile();
    auto tmpCoverFile = fs::path("/proc") / std::to_string( ::getpid())  / "fd" / std::to_string(fileno(tmpf));

    fs::path tmpCoverPath{tmpCoverFile};//{std::tmpnam(nullptr)}; //std::tmpnam(nullptr)

    std::fstream coverStream{tmpCoverPath, std::ios::out|std::ios::in|std::ios::trunc };
    //coverStream.unsetf(std::ios::skipws);

    if(not coverStream.is_open())
    {
        LOG(Log::Level::Error,
            L_UNIT("mediaComponent"),
            L_MESSAGE("Could not create file for cover image" ));

    }
    else{
        const std::string command{ std::string{"curl -L --output - "}.append( jpgUrl).append(" 2>/dev/null | magick - png: 2> /dev/null > ") .append(fs::absolute(tmpCoverPath)) };

        auto loadImageResult = std::system( command.c_str());

        if(loadImageResult != 0)
        {
            LOG(Log::Level::Error,
                L_UNIT("mediaComponent"),
                L_MESSAGE_F("Could not download cover: %s", C_STR(jpgUrl) ));

            if(std::fclose(tmpf))
            {
                LOG(Log::Level::Error,
                    L_UNIT("mediaComponent"),
                    L_MESSAGE_F("Could not cleanup downloaded cover: %s", C_STR(jpgUrl) ));
            }
            return  imageBuffer;
        }

        auto picSize { std::filesystem::file_size(tmpCoverPath) };
        imageBuffer.reserve(picSize);

        // read the data:
        imageBuffer.insert(imageBuffer.begin(),
                           std::istreambuf_iterator<char>(coverStream),
                           std::istreambuf_iterator<char>());
        coverStream.close();
    }

    if(std::fclose(tmpf))
    {
       LOG(Log::Level::Error,
           L_UNIT("mediaComponent"),
           L_MESSAGE_F("Could not cleanup downloaded cover: %s", C_STR( jpgUrl ) ));
    }
    return  imageBuffer;
}

bool MediaComponent::needCoverNodeUpdate(const std::string_view& playerDbusId, const std::string_view newCoverUrl)
{
    bool needUpdate{true};

    UA_Variant oldValue;
    UA_Server_readValue(server, NODEID_COVERURL(playerDbusId), &oldValue);

    if(UA_Variant_hasScalarType(&oldValue, &UA_TYPES[UA_TYPES_STRING]))
    {
        //UA_String oldUrl = *(UA_String *) value.data;
        std::string_view oldCoverUrl{reinterpret_cast<const char*>(((UA_String *) oldValue.data)->data), ((UA_String *) oldValue.data)->length};

        needUpdate = not (oldCoverUrl == newCoverUrl);
    }
    UA_Variant_deleteMembers(&oldValue);

    return  needUpdate;
}

void MediaComponent::writeCoverNode(UA_NodeId nId, const std::vector<char> && imageData)
{
    UA_Variant newValue;
    UA_ByteString img{.length = imageData.size(), .data=(uint8_t*)imageData.data() };

    UA_Variant_setScalar(&newValue, &img, &UA_TYPES[UA_TYPES_BYTESTRING]);
    UA_Server_writeValue(server, nId, newValue);
}


void MediaComponent::writeCoverUrlNode(const std::string_view& playerDbusId, const std::string_view& coverUrl)
{
    UA_String newUrl = UA_STRING(C_STR(coverUrl));
    UA_Variant newValue;

    UA_Variant_setScalar(&newValue, &newUrl, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_writeValue(server, NODEID_COVERURL(playerDbusId), newValue);
}

void MediaComponent::startCoverImageUpdate(const std::string_view& playerDbusId, const std::string_view& coverUrl)
{
    coverLaoders.erase(playerDbusId);

    auto loader{ std::async(std::launch::async,
                            loadCoverFromJpgUrl, coverUrl )
               };

    if(loader.valid())
    {
        coverLaoders.emplace(
                    playerDbusId,
                    std::pair{coverUrl,std::move(loader)});
    }
}

void MediaComponent::processCoverImageUpdate(const std::string_view& playerDbusId)
{
    auto it { coverLaoders.find(playerDbusId) };

    if(it == coverLaoders.end()){ return; }

    const auto& url = it->second.first;
    auto& loader = it->second.second;

    if( std::future_status::ready != loader.wait_for(std::chrono::seconds(0))){
        return;
    }

    //  auto imageData{  };

    writeCoverNode(NODEID_COVER(playerDbusId), std::move(loader.get()) );

    coverLaoders.erase(playerDbusId);
}


bool MediaComponent::addMediaPlayer(const std::string_view &dbusMediaPlayerName)
{
    if(memorizeMediaPlayerName(dbusMediaPlayerName))
    {
        addMediaPlayerNode(dbusMediaPlayerName);

        LOG(Log::Level::Informational,
            L_UNIT("mediaComponent"),
            L_MESSAGE_F("MediaPlayer found: %s", C_STR(dbusMediaPlayerName) ));
        return true;
    }
    return  false;
}

void MediaComponent::addMediaPlayerNode(const std::string_view & dbusMediaPlayerName)
{
    auto it =mediaPlayerIDs.find(dbusMediaPlayerName);
    if(it == mediaPlayerIDs.end())
    {
        return;
    }
    const std::string & mediaPlayerKey = it->first;
    const std::string & mediaPlayerName = it->second;


    UA_NodeId playerNode = UA_NODEID_NULL;
    UA_NodeId node = UA_NODEID_NULL;

    //Add Nodes             //----------------------------------------------------
    {

        //Add Player Object     //------------------------------------------------
        {
            UA_ObjectAttributes playerAttr = UA_ObjectAttributes_default;
            playerAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"), C_STR(mediaPlayerName));

            playerNode = NODEID_PLAYER_A(mediaPlayerKey);
            UA_Server_addObjectNode(server, playerNode, mediaControlNode,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                    UA_QUALIFIEDNAME(namespaceIndex, C_STR(mediaPlayerName)),
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE), playerAttr, nullptr, nullptr);
        }
        //------------------------------------------------------------------------

        //Add Artist Property   //------------------------------------------------
        {
            UA_VariableAttributes artistAttr = UA_VariableAttributes_default;
            artistAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"), U8("Artist"));
            artistAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;

            UA_String artistName = UA_STRING(U8(""));
            UA_Variant_setScalar(&artistAttr.value, &artistName, &UA_TYPES[UA_TYPES_STRING]);

            node = NODEID_ARTIST_A(mediaPlayerKey);
            UA_Server_addVariableNode(server, node, playerNode,
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY),
                                      UA_QUALIFIEDNAME(namespaceIndex, U8("Artist")),
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), artistAttr, NULL, NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------

        //Add Title Property    //------------------------------------------------
        {
            UA_VariableAttributes titleAttr = UA_VariableAttributes_default;
            titleAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"), U8("Title"));
            titleAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;

            UA_String titleName = UA_STRING(U8(""));
            UA_Variant_setScalar(&titleAttr.value, &titleName, &UA_TYPES[UA_TYPES_STRING]);

            node = NODEID_TITLE_A(mediaPlayerKey);
            UA_Server_addVariableNode(server, node, playerNode,
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY),
                                      UA_QUALIFIEDNAME(namespaceIndex, U8("Title")),
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_PROPERTYTYPE), titleAttr,
                                      nullptr, NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------

        //Add CoverArt Property    //------------------------------------------------
        {
            UA_VariableAttributes coverAttr = UA_VariableAttributes_default;
            coverAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"), U8("Cover"));
            coverAttr.dataType =  {0, UA_NODEIDTYPE_NUMERIC, {UA_NS0ID_IMAGE}};//UA_TYPES[UA_TYPES_BYTESTRING].typeId;
            coverAttr.valueRank = UA_VALUERANK_SCALAR;

            UA_ByteString img = UA_BYTESTRING(U8(""));
            UA_Variant_setScalar(&coverAttr.value, &img, &UA_TYPES[UA_TYPES_BYTESTRING]);

            node = NODEID_COVER_A(mediaPlayerKey);
            UA_Server_addVariableNode(server, node, playerNode,
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                                      UA_QUALIFIEDNAME(namespaceIndex, U8("Cover")),
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), coverAttr,
                                      nullptr, NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------

        //Add CoverUrl Property   //------------------------------------------------
        {
            UA_VariableAttributes coverUrlAttr = UA_VariableAttributes_default;
            coverUrlAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"), U8("CoverUrl"));
            coverUrlAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;

            UA_String artistName = UA_STRING(U8(""));
            UA_Variant_setScalar(&coverUrlAttr.value, &artistName, &UA_TYPES[UA_TYPES_STRING]);

            node = NODEID_COVERURL_A(mediaPlayerKey);
            UA_Server_addVariableNode(server, node, playerNode,
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_HASPROPERTY),
                                      UA_QUALIFIEDNAME(namespaceIndex, U8("CoverUrl")),
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), coverUrlAttr, NULL, NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------


        //Add Next Method       //------------------------------------------------
        {
            UA_MethodCallback nextTrack = [](UA_Server *server, const UA_NodeId *sessionId,
                    void *sessionContext, const UA_NodeId *methodId,
                    void *methodContext, const UA_NodeId *objectId,
                    void *objectContext, size_t inputSize,
                    const UA_Variant *input, size_t outputSize,
                    UA_Variant *output) -> UA_StatusCode
            {
                Context* context{CONTEXT_PTR(methodContext)};

                DMU::nextTrack(context->first->bus, context->second);

                return UA_STATUSCODE_GOOD;
            };

            UA_MethodAttributes nextAttr = UA_MethodAttributes_default;
            nextAttr.description = UA_LOCALIZEDTEXT(U8("en-US"),U8("Next Track"));
            nextAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"),U8("Next"));
            nextAttr.executable = true;
            nextAttr.userExecutable = true;

            node = NODEID_NEXT_A(mediaPlayerKey);
            int r = UA_Server_addMethodNode(server, node, playerNode,
                                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                                            UA_QUALIFIEDNAME(namespaceIndex, U8("Next")),
                                            nextAttr, nextTrack ,
                                            0, NULL, 0, NULL, makeContext(mediaPlayerKey) , NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------

        //Add Previous Method   //------------------------------------------------
        {
            UA_MethodCallback previousTrack = [](UA_Server *server, const UA_NodeId *sessionId,
                    void *sessionContext, const UA_NodeId *methodId,
                    void *methodContext, const UA_NodeId *objectId,
                    void *objectContext, size_t inputSize,
                    const UA_Variant *input, size_t outputSize,
                    UA_Variant *output) -> UA_StatusCode
            {
                Context* context{CONTEXT_PTR(methodContext)};

                DMU::previousTrack(context->first->bus, context->second);

                return UA_STATUSCODE_GOOD;
            };

            UA_MethodAttributes previousAttr = UA_MethodAttributes_default;
            previousAttr.description = UA_LOCALIZEDTEXT(U8("en-US"),U8("Previous Track"));
            previousAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"),U8("Previous"));
            previousAttr.executable = true;
            previousAttr.userExecutable = true;

            node = NODEID_PREVIOUS_A(mediaPlayerKey);
            int r = UA_Server_addMethodNode(server, node, playerNode,
                                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                                            UA_QUALIFIEDNAME(namespaceIndex, U8("Previous")),
                                            previousAttr, previousTrack ,
                                            0, NULL, 0, NULL, makeContext(mediaPlayerKey) , NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------

        //Add PlayPause Method  //------------------------------------------------
        {
            UA_MethodCallback playPause = [](UA_Server *server, const UA_NodeId *sessionId,
                    void *sessionContext, const UA_NodeId *methodId,
                    void *methodContext, const UA_NodeId *objectId,
                    void *objectContext, size_t inputSize,
                    const UA_Variant *input, size_t outputSize,
                    UA_Variant *output) -> UA_StatusCode
            {
                Context* context{CONTEXT_PTR(methodContext)};

                DMU::playPause(context->first->bus, context->second);

                return UA_STATUSCODE_GOOD;
            };

            UA_MethodAttributes playPauseAttr = UA_MethodAttributes_default;
            playPauseAttr.description = UA_LOCALIZEDTEXT(U8("en-US"),U8("Play/Pause"));
            playPauseAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"),U8("PlayPause"));
            playPauseAttr.executable = true;
            playPauseAttr.userExecutable = true;

            node = NODEID_PLAYPAUSE_A(mediaPlayerKey);
            int r = UA_Server_addMethodNode(server, node, playerNode,
                                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                                            UA_QUALIFIEDNAME(namespaceIndex, U8("PlayPause")),
                                            playPauseAttr, playPause ,
                                            0, NULL, 0, NULL, makeContext(mediaPlayerKey) , NULL);
            UA_NodeId_deleteMembers(&node);
        }
        //------------------------------------------------------------------------
    }
    //----------------------------------------------------------------------------
    UA_NodeId_deleteMembers(&playerNode);
}


void MediaComponent::removeMediaPlayer(const  std::string_view &dbusMediaPlayerName)
{
    UA_StatusCode status{UA_STATUSCODE_GOOD};

    coverLaoders.erase(dbusMediaPlayerName);

    if( removeMediaPlayerNode(dbusMediaPlayerName) )
    {
        auto it = std::find_if(begin(mediaPlayerIDs), end(mediaPlayerIDs), [&dbusMediaPlayerName](const auto& pair)
        {
            return pair.first == dbusMediaPlayerName;
        });
        if(it != end(mediaPlayerIDs))
        {
            LOG(Log::Level::Informational,
                L_UNIT("mediaComponent"),
                L_MESSAGE_F("MediaPlayer removed: %s", C_STR(dbusMediaPlayerName) ));
            mediaPlayerIDs.erase( it ); //makes dbusMediaPlayerName dangling
        }
    }
    else
    {
        std::cout << "error deleting Player Node (" << status << "): " << dbusMediaPlayerName << std::endl;
    }
}

bool MediaComponent::removeMediaPlayerNode(const std::string_view &dbusMediaPlayerName)
{
    auto deleteNode =
            [this](UA_NodeId nId)
    {
        UA_StatusCode ret{UA_STATUSCODE_GOOD};

        Context* context { nullptr };
        ret|=UA_Server_getNodeContext(server,nId, (void**)&context );
        deleteContext(context);

        ret|=UA_Server_deleteNode(server, nId, true);

        return ret;
    };

    UA_StatusCode ret{UA_STATUSCODE_GOOD};

    ret|=deleteNode(NODEID_PREVIOUS( dbusMediaPlayerName));
    ret|=deleteNode(NODEID_NEXT(     dbusMediaPlayerName));
    ret|=deleteNode(NODEID_PLAYPAUSE(dbusMediaPlayerName));

    ret|=deleteNode(NODEID_ARTIST(   dbusMediaPlayerName));
    ret|=deleteNode(NODEID_TITLE(    dbusMediaPlayerName));
    ret|=deleteNode(NODEID_COVER (   dbusMediaPlayerName));
    ret|=deleteNode(NODEID_COVERURL( dbusMediaPlayerName));

    ret|=deleteNode(NODEID_PLAYER(   dbusMediaPlayerName));

    return (ret == UA_STATUSCODE_GOOD);
}


void MediaComponent::updateMediaPlayers()
{
    {std::vector<std::string_view> closedPlayers( getPlayerKeys() ); //assume all players are not available

        DMU::findMediaPlayers(bus, [&](const auto& player)
        {
            if(not addMediaPlayer(player))
            {
                //player is available and should not be removed
                auto it = std::find(begin(closedPlayers), end(closedPlayers), player);
                if(it != end(closedPlayers)){
                    closedPlayers.erase( it );
                }
            }
        });

        for(const auto& player : closedPlayers)
        {
            removeMediaPlayer(player);
        }
    }
    return;
}


//true -> saved, false -> allrady memorized, exception -> something went wrong
bool MediaComponent::memorizeMediaPlayerName(const std::string_view & mediaPlayerName, MediaPlayerIdMap::iterator* insertedElement_out)
{
    std::pair<MediaPlayerIdMap::iterator, bool> res = mediaPlayerIDs.emplace( std::string(mediaPlayerName), DMU::dbusName2CleanName(mediaPlayerName) );

    if(insertedElement_out)
    {
        *insertedElement_out=res.first;
    }

    return res.second;
}

std::vector< std::string_view > MediaComponent::getPlayerKeys() const
{
    std::vector< std::string_view > keys;
    for(const auto& pair: mediaPlayerIDs)
    {
        keys.push_back( pair.first );
    }
    return keys;
}


void MediaComponent::deleteContext(MediaComponent::Context* context)
{
    if(context)
    {
        delete context;
    }
}

MediaComponent::Context * MediaComponent::makeContext(std::string_view content)
{
    return new Context{this, content};
}


