﻿#include "DbusMediaUtil.h"

#include <functional>
#include <string>
#include <string_view>

#include "Log.h"

#include <exception>
//#include <stdexcept>

const std::string DbusMediaUtil::baseMediaPlayerName{u8"org.mpris.MediaPlayer2."};
const std::string_view DbusMediaUtil::path{"/org/mpris/MediaPlayer2"};
const std::string_view DbusMediaUtil::interface{"org.mpris.MediaPlayer2.Player"};


const void* readNextValue(sd_bus_message* msg);
int enterVariantRec(sd_bus_message* msg);
void exitContainer(sd_bus_message* msg, int n=1);

class VariantGuard;

class VariantGuard{
public:
    sd_bus_message* m;
    int n;
    VariantGuard(sd_bus_message* msg):m(msg),n(0){enterNextVariant();}
    void enterNextVariant(){n+=enterVariantRec(m);};
    ~VariantGuard(){exitContainer(m, n);}
};


#define NOT_NULL_STR( ptr )     ( ((ptr) == nullptr) ? u8"" : (ptr) )
#define NULL_STR( ptr )         ( ((*NOT_NULL_STR(ptr)) == '\0' ? "null" : (ptr)) )

//#define DEBUG_PRINT
#ifdef DEBUG_PRINT
#define PRINT_TYPE(txt, dbus_msg) \
{   const char* contentType__; char containerType__[2]{'\0', '\0'}; \
    sd_bus_message_peek_type( (dbus_msg) , &containerType__[0], &contentType__); \
    std::cout << (txt) << " : " << NULL_STR(containerType__) << "[ " << NULL_STR(contentType__)<< " ]" << std::endl;\
    }
#else
#define PRINT_TYPE(txt, dbus_msg)
#endif

void printNextType(sd_bus_message* msg)
{
    const char* contentType;
    char containerType[2]{'\0', '\0'};

    sd_bus_message_peek_type(msg,&containerType[0],&contentType);

    std::cout << "container: " << NULL_STR( containerType )
              << " content: "  << NULL_STR(contentType) << "\n";
}

const void ignoreNextValue(sd_bus_message* msg)
{
    int r{0};
    const char* contentType;
    char containerType[2]{'\0', '\0'};

    sd_bus_message_peek_type(msg,&containerType[0],&contentType);
    r= sd_bus_message_skip(msg, containerType);
    r= sd_bus_message_exit_container(msg);
}

const void* readNextValue(sd_bus_message* msg)
{
    const void* content{nullptr};

    int r{0};
    const char* contentType;
    char containerType[2]{'\0', '\0'};

    VariantGuard variantGuard(msg);

    sd_bus_message_peek_type(msg,&containerType[0],&contentType);

    r = sd_bus_message_read_basic(msg, containerType[0], &content); //SD_BUS_TYPE_STRING

    r = sd_bus_message_exit_container(msg);

    return  content;
}

template < typename ElementCallback >
void for_each_array(sd_bus_message* msg, ElementCallback elemCall)
{
    int r{0};
    const char* contentType;
    char containerType[2]{'\0', '\0'};

    PRINT_TYPE("arry___1",msg);
    VariantGuard variantExitGuard(msg);

    r = sd_bus_message_peek_type(msg,&containerType[0],&contentType);

    if(containerType[0] != SD_BUS_TYPE_ARRAY)
    {
        PRINT_TYPE("arry_err",msg);

        throw std::bad_typeid();
    }

    r = sd_bus_message_enter_container(msg, containerType[0], contentType);
    r = sd_bus_message_peek_type(msg,&containerType[0],&contentType);
    //sd_bus_message_read_basic
    PRINT_TYPE("arry___2",msg);
    for(const void * elem = nullptr; (r=sd_bus_message_read_basic(msg, containerType[0], &elem))>=0;)
    {
        PRINT_TYPE("arry__f1",msg);
        std::invoke(elemCall, msg, elem);
        PRINT_TYPE("arry__f2",msg);
        r = sd_bus_message_exit_container(msg); //exit value, enterd by sd_bus_message_read_basic(..)
    }
    r = sd_bus_message_exit_container(msg); //exit array

    //~StackLeaver()
}


template < typename DictCallback >
void for_each_dict(sd_bus_message* msg, DictCallback elemCall)
{
    int r{0};
    const char* contentType;
    char containerType[2]{'\0', '\0'};

    PRINT_TYPE("dict___1",msg);
    VariantGuard variantExitGuard(msg);

    r = sd_bus_message_peek_type(msg,&containerType[0],&contentType);

    if(containerType[0] != SD_BUS_TYPE_ARRAY)
    {
        PRINT_TYPE("dict_err",msg);
        throw std::bad_typeid();
    }

    r = sd_bus_message_enter_container(msg, containerType[0], contentType);

    r = sd_bus_message_peek_type(msg,&containerType[0],&contentType);
    PRINT_TYPE("dict___2",msg);
    for(const void * elem = nullptr ; (r = sd_bus_message_enter_container(msg, SD_BUS_TYPE_DICT_ENTRY, contentType ))>0; )
    {
        PRINT_TYPE("dict__f1",msg);
        std::invoke(elemCall, msg);
        PRINT_TYPE("dict__f2",msg);
    }
    r = sd_bus_message_exit_container(msg);

    //~StackLeaver()
}


int enterVariantRec(sd_bus_message* msg)
{
    int r{0};
    const char* contentType;
    char containerType[2]{'\0', '\0'};

    int leaveContainer{0};

    do{
        sd_bus_message_peek_type(msg,&containerType[0],&contentType);

        if(containerType[0]=='v')
        {
            r = sd_bus_message_enter_container(msg, SD_BUS_TYPE_VARIANT, contentType);
            leaveContainer++;
        }
        else
        {
            break;
        }
    }while(r >= 0);

    if(r < 0){ throw std::overflow_error("entering container failed"); }

    return  leaveContainer;
}

void exitContainer(sd_bus_message* msg, int n)
{
    while(n-- > 0)
    {
        if(sd_bus_message_exit_container(msg) < 0)
        {
            throw std::underflow_error("leaving container failed");
        }
    }
}


std::string DbusMediaUtil::dbusName2CleanName(const std::string_view & dbusMediaPlayerName)
{
    return std::string{ dbusMediaPlayerName.substr(DbusMediaUtil::baseMediaPlayerName.length(), std::string::npos) };
}

std::string DbusMediaUtil::name2DbusName(const std::string_view & mediaPlayerName)
{
    return { DbusMediaUtil::baseMediaPlayerName + C_STR(mediaPlayerName) };
}


//template <typename ArtistCallback, typename TitleCallback>
void DbusMediaUtil::grepMetadataDbus(sd_bus* bus, const std::string medisPlayerDbusId,
                                     std::function<void(const std::vector<std::string_view>&)> artistFn,
                                     std::function<void(const std::string_view&)> titleFn,
                                     std::function<void(const std::string_view&)> coverUrlFn)
{

    /* see:
     * https://www.freedesktop.org/wiki/Specifications/mpris-spec/metadata/
     *
     * https://specifications.freedesktop.org/mpris-spec/latest/Track_List_Interface.html#Mapping:Metadata_Map
     *
     * https://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html#Property:Metadata
     *
     * https://specifications.freedesktop.org/mpris-spec/latest/
     *
     * https://specifications.freedesktop.org/mpris-spec/latest/Player_Interface.html
     *
     * $ busctl --user get-property org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player Metadata
     * */

    const std::string_view property{"Metadata"};

    sd_bus_error err = SD_BUS_ERROR_NULL;
    sd_bus_message* msg = nullptr;
    int r;

    r = sd_bus_get_property(bus,
                            medisPlayerDbusId.c_str(),
                            path.data(),
                            interface.data(),
                            property.data(),
                            &err,
                            &msg,
                            "a{sv}"
                            );

    if(r<0)
    {
        sd_bus_error_free(&err);
        sd_bus_message_unref(msg);
        return;
    }

    /* //alternative way
    r=sd_bus_call_method(bus,
                         medisPlayerDbusId.c_str(),
                         path.c_str(),
                         "org.freedesktop.DBus.Properties",
                         "Get",
                         &err,
                         &msg,
                         "ss",
                         interface.c_str(),
                         property.c_str()
                         ); //returns va{sv}
    r = sd_bus_message_enter_container(msg, SD_BUS_TYPE_VARIANT, "a{sv}");
    //*/

    VariantGuard variantGuard(msg);

    PRINT_TYPE(("meta___1"),msg);
    for_each_dict(msg,[&](auto msg)
    {

        PRINT_TYPE("meta___2",msg);
        const char* contentType;
        char containerType[2]{'\0', '\0'};
        int r{0};

        {
            const char* key_ptr = static_cast<const char*>(readNextValue(msg));

            std::string_view key{(key_ptr)};


            if (key == "xesam:title")
            { //see: https://www.freedesktop.org/wiki/Specifications/mpris-spec/metadata/#index18h4

                const char* title = nullptr;

                title=static_cast<const char*>(readNextValue(msg));

                std::invoke(titleFn, (std::string_view{title}) );
            }
            else if(key == "xesam:artist")
            { //see: https://www.freedesktop.org/wiki/Specifications/mpris-spec/metadata/#index6h4

                //*
                std::vector<std::string_view> artists;


                for_each_array(msg, [&artists](auto msg, auto& artist)
                {
                    artists.push_back(std::string_view{ static_cast<const char*>(artist)});
                } );

                std::invoke( artistFn, (static_cast<const std::vector<std::string_view>>(artists)) );

            }
            else if(key == "mpris:artUrl")
            {
                const char* coverUrl = nullptr;

                coverUrl=static_cast<const char*>(readNextValue(msg));

                std::invoke(coverUrlFn, (std::string_view{coverUrl}) );
            }
            else
            { //ignore non relevant information

                ignoreNextValue(msg);
            }

            // r = sd_bus_message_exit_container(msg);

        }
    });

    //cleanup
    sd_bus_error_free(&err);
    sd_bus_message_unref(msg);
}


bool DbusMediaUtil::isMediaPlayer(const char* mediaPlayerName)
{
    if(DbusMediaUtil::baseMediaPlayerName.compare(0, DbusMediaUtil::baseMediaPlayerName.length(), mediaPlayerName, DbusMediaUtil::baseMediaPlayerName.length()) == 0)
    {
        return true;
    }
    return  false;
}

int DbusMediaUtil::connectToUserSession( sd_bus* & bus)
{
    if(bus != nullptr)
    { //bus allready in use
        return EBUSY;
    }

    int r;

    r = sd_bus_open_user(&bus);

    if (r < 0) {
        sd_bus_unref(bus);
    }

    return r;
}


//template<typename MediaPlayerFound>
void DbusMediaUtil::findMediaPlayers(sd_bus* bus, std::function<void(const std::string_view&)> callback)
{
    if(bus == nullptr){return;}

    int r;
    char** names;

    r = sd_bus_list_names(bus,&names, NULL );

    for(char** name=names; name && *name ; name++)
    {
        if(isMediaPlayer(*name)){
            std::invoke(callback, std::string_view{*name});
        }
        std::free(*name);
    }
    std::free(names);
}

void DbusMediaUtil::nextTrack(sd_bus *bus, const std::string_view &playerDbusId)
{
    std::string_view method{"Next"};

    call_NoArgsNoResult(bus, playerDbusId, method);
}

void DbusMediaUtil::previousTrack(sd_bus *bus, const std::string_view &playerDbusId)
{
    std::string_view method{"Previous"};

    call_NoArgsNoResult(bus, playerDbusId, method);
}

void DbusMediaUtil::playPause(sd_bus *bus, const std::string_view &playerDbusId)
{
    std::string_view method{"PlayPause"};

    call_NoArgsNoResult(bus, playerDbusId, method);
}


int DbusMediaUtil::call_NoArgsNoResult(sd_bus *bus, const std::string_view &playerDbusId, const std::string_view &method)
{
    sd_bus_error err = SD_BUS_ERROR_NULL;
    sd_bus_message* msg = nullptr;

    int r {0};

    r=sd_bus_call_method(bus,
                         C_STR(playerDbusId),
                         C_STR(path),
                         C_STR(interface),
                         C_STR(method),
                         &err, &msg, "");

    sd_bus_error_free(&err);
    sd_bus_message_unref(msg);

    return r;
}

