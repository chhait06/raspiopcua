﻿#ifndef DBUSMEDIAUTIL_H
#define DBUSMEDIAUTIL_H

#include <open62541.h>

#include <component_handle.h>

#include <systemd/sd-bus.h>

#include <vector>
#include <map>
#include <string>
#include <string_view>


#include <functional>



class DbusMediaUtil
{
public:
    const static std::string baseMediaPlayerName;
    const static std::string_view path;
    const static std::string_view interface;

    static void findMediaPlayers(sd_bus* bus, std::function<void(const std::string_view&)> callback);


    static void grepMetadataDbus(sd_bus* bus, const std::string medisPlayerDbusId,
                                 std::function<void(const std::vector<std::string_view>&)> artistFn,
                                 std::function<void(const std::string_view&)> titleFn,
                                 std::function<void(const std::string_view&)> coverUrlFn);

    static bool isMediaPlayer(const char* mediaPlayerName);

    static int connectToUserSession( sd_bus* & bus);

    static std::string dbusName2CleanName(const std::string_view & dbusMediaPlayerName);

    static std::string name2DbusName(const std::string_view & mediaPlayerName);



    static void nextTrack(sd_bus* bus, const std::string_view& playerDbusId);
    static void previousTrack(sd_bus* bus, const std::string_view& playerDbusId);
    static void playPause(sd_bus* bus, const std::string_view& playerDbusId);
private:
    static int call_NoArgsNoResult(sd_bus* bus, const std::string_view& playerDbusId, const std::string_view& method);



};
#endif // DBUSMEDIAUTIL_H
