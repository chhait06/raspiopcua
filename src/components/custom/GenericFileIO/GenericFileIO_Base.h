#ifndef GENERICFILEIO_BASE_H
#define GENERICFILEIO_BASE_H

#include <open62541.h>

#include <component_base.h>
#include <component_handle.h>

//don't forget!
//extern "C" {
//struct ComponentApi getComponentApiHandle();
//}

struct ComponentApi getGenericFileIOComponenApi();


int initFn(struct ComponentHandle * comp);

int notesFn(struct ComponentHandle * comp);

int startFn(struct ComponentHandle * comp);

int iterFn(struct ComponentHandle * comp);

int stopFn(struct ComponentHandle * comp);

int deleteFn(struct ComponentHandle * comp);

#endif // GENERICFILEIO_BASE_H
