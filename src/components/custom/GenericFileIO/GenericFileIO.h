#ifndef GENERICFILEIO_H
#define GENERICFILEIO_H

#include <component_base.h>
#include <component_handle.h>

#include <filesystem>
#include <string_view>
#include <fstream>

#include <iostream>

enum class FileDataType
{
    INVALID=-1,

    Text = UA_TYPES_STRING,
    Number = UA_TYPES_INT32,
    Bool = UA_TYPES_BOOLEAN
};
FileDataType Text2FileDataType(std::string_view datatype_text);

struct FileDescription
{
    FileDataType type{FileDataType::INVALID};
    std::filesystem::path path{};
    bool writable{false};
    bool keep_open{false};

    std::string browsename{};
};

class GenericFileIO
{

public:
    GenericFileIO(UA_Server* server, FileDescription file_description);
    GenericFileIO(const GenericFileIO& f) = delete;
     GenericFileIO(GenericFileIO&& f);
    ~GenericFileIO();

public:
    void start();
    void iter();
    void stop();

public:
    int onReadNode(UA_DataValue *value);
    int onWriteNode(const UA_DataValue *newValue);

private:
    int beforeRead();
    int afterRead();

    int beforeWrite();
    int afterWrite();


private:
    UA_Server* srv;
    FileDescription fd;

    UA_NodeId fileNode;

    bool started;

    std::fstream fileStream;

    std::ios::openmode mode;;


};

#endif // GENERICFILEIO_H
