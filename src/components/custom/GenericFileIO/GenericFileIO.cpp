#include "GenericFileIO.h"

#include <map>
#include <typeinfo>
#include <iostream>
#include <utility>
#include <string>
#include <sstream>
#include <cpptoml.h>

#include <Log.h>
#include <array>

void setDefaultVariantValue(UA_Variant* var, FileDataType type);

FileDataType UaType2FileDataType(const UA_DataType* datatype_ua);

extern "C"
UA_StatusCode readNode(UA_Server *server,
                       const UA_NodeId *sessionId, void *sessionContext,
                       const UA_NodeId *nodeId, void *nodeContext,
                       UA_Boolean sourceTimeStamp,
                       const UA_NumericRange *range, UA_DataValue *value);

FileDataType Text2FileDataType(std::string_view datatype_text)
{
    static std::map<std::string_view, FileDataType> t2d{
        {{"Text"},{FileDataType::Text}},
        {{"text"},{FileDataType::Text}},
        {{"TEXT"},{FileDataType::Text}},

        {{"Number"},{FileDataType::Number}},
        {{"number"},{FileDataType::Number}},
        {{"NUMBER"},{FileDataType::Number}},

        {{"Bool"},{FileDataType::Bool}},
        {{"bool"},{FileDataType::Bool}},
        {{"BOOL"},{FileDataType::Bool}},
    };

    auto result = t2d.find(datatype_text);

    if(result == t2d.end()){
        return FileDataType::INVALID;
    }

    return result->second;
}

FileDataType UaType2FileDataType(const UA_DataType* datatype_ua)
{
    static std::map<const UA_DataType*, FileDataType> u2d
    {
        {{&UA_TYPES[UA_TYPES_STRING]},{FileDataType::Text}},
        {{&UA_TYPES[UA_TYPES_INT32]},{FileDataType::Number}},
        {{&UA_TYPES[UA_TYPES_BOOLEAN]},{FileDataType::Bool}}
    };

    auto result = u2d.find(datatype_ua);

    if(result == u2d.end())
    {
        return FileDataType::INVALID;
    }

    return result->second;
}


UA_StatusCode readNode(UA_Server *server,
                       const UA_NodeId *sessionId, void *sessionContext,
                       const UA_NodeId *nodeId, void *nodeContext,
                       UA_Boolean sourceTimeStamp,
                       const UA_NumericRange *range, UA_DataValue *value)
{
    if(nodeContext == nullptr){
        return UA_STATUSCODE_BADSTRUCTUREMISSING;
    }

    UA_StatusCode ret = UA_STATUSCODE_GOOD;

    GenericFileIO* fileIO = static_cast<GenericFileIO*>(nodeContext);

    value->hasStatus = true;

    if (range)
    {

        value->status = UA_STATUSCODE_BADINDEXRANGEINVALID;
        return UA_STATUSCODE_GOOD;
    }

    ret = fileIO->onReadNode(value);

    if(ret == UA_STATUSCODE_GOOD){
        value->hasValue = true;
    }

    if (sourceTimeStamp) {
        value->hasSourceTimestamp = true;
        value->sourceTimestamp = UA_DateTime_now();
    }
    value->status = ret;

    return UA_STATUSCODE_GOOD;
}

UA_StatusCode writeNode(UA_Server *server, const UA_NodeId *sessionId,
                        void *sessionContext, const UA_NodeId *nodeId,
                        void *nodeContext, const UA_NumericRange *range,
                        const UA_DataValue *newValue)
{
    if(nodeContext == nullptr){
        return UA_STATUSCODE_BADSTRUCTUREMISSING;
    }

    GenericFileIO* fileIO = static_cast<GenericFileIO*>(nodeContext);

    if (newValue->hasValue)
    {
        fileIO->onWriteNode(newValue);
    }
    return UA_STATUSCODE_GOOD;

}

GenericFileIO::GenericFileIO(UA_Server* server, FileDescription file_description)
    :srv{server}
    ,fd{file_description}
    ,fileNode{UA_NODEID_NULL}
    ,fileStream{}
    ,started{false}
    ,mode{std::ios::in}
{
    if(fd.writable){
        //auto file_status = std::filesystem::status(fd.path);
        //file_status.permissions()
        //have to permissions give rwx for owser group and other, but no "can i write or read? yes or no?" answer
        mode |=  std::ios::out;
    }

    UA_VariableAttributes fileNodeAttr = UA_VariableAttributes_default;
    fileNodeAttr.displayName = UA_LOCALIZEDTEXT(U8("en-US"), C_STR(fd.browsename));
    fileNodeAttr.dataType = UA_TYPES[(int)fd.type].typeId;
    fileNodeAttr.accessLevel = UA_ACCESSLEVELMASK_READ | (fd.writable ? UA_ACCESSLEVELMASK_WRITE : 0);

    UA_DataSource dataSource;
    dataSource.read = ::readNode;
    dataSource.write = ::writeNode;

    UA_Server_addDataSourceVariableNode(
                server, UA_NODEID_NULL,//requested node id
                UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER), //parent node
                UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),       //referencetype
                UA_QUALIFIEDNAME(0, C_STR(fd.browsename)), //browsename
                UA_NODEID_NULL, //typedefinition
                fileNodeAttr,   //attribute
                dataSource, //datasource
                this,//nodecontext
                &fileNode);
}

GenericFileIO::GenericFileIO(GenericFileIO && f )
    :srv{std::exchange(f.srv, nullptr)}
    ,fd{std::move(f.fd)}
    ,fileNode{std::exchange(f.fileNode, UA_NODEID_NULL)}
    ,fileStream{std::move(f.fileStream)}
    ,started{std::exchange(f.started, false)}
    ,mode{f.mode}
{
    UA_Server_setNodeContext(srv, fileNode, this);
}

GenericFileIO::~GenericFileIO()
{
    stop();
    UA_NodeId_deleteMembers(&fileNode);
}

void GenericFileIO::start()
{
    if(!std::filesystem::exists(fd.path))
    {
        LOG(Log::Level::Notice,
            L_UNIT_F("GenericFileIoComponent:%s", "start"),
            L_MESSAGE("File does not exist!"),
            L_ELEM("FILIE_IO_PATH", fd.path.c_str()));

        return;
    }


    fileStream.open(fd.path.u8string().c_str(), mode );

    auto failed =fileStream.fail();
    auto bad = fileStream.bad();
    auto good = fileStream.good();

    if(!good)
    {
        LOG(Log::Level::Notice,
            L_UNIT_F("GenericFileIoComponent:%s", "start"),
            L_MESSAGE("Could not open file! Do you have access permissions?"),
            L_ELEM("FILIE_IO_PATH", fd.path.c_str()),
            L_ELEM("FILIE_IO_WANTS_WRITE_ACCESS", (fd.writable?"true":"false"))
            );

        fileStream.close();
    }
    else{
        started=true;
    }

    if(!fd.keep_open){
        fileStream.close();
    }


    // std::clog << "opening file was: good[" << good << "] bad[" << bad << "] failed[" << failed << "]" << "\n";
}


void GenericFileIO::iter()
{
    //nothing to do
}

void GenericFileIO::stop()
{

    if(fileStream.is_open())
    {
        fileStream.close();
    }
}

int GenericFileIO::onReadNode(UA_DataValue *value)
{
    std::basic_string<char> content{};

    if(!beforeRead())
    {
        return UA_STATUSCODE_BADNODATAAVAILABLE;
    }

    try{
        auto fsize = std::filesystem::file_size(fd.path);
        content.reserve(fsize);
    }
    catch(...)
    {
        return UA_STATUSCODE_BADUSERACCESSDENIED;
    }


    fileStream.seekg(0, std::ios::beg);

    content.insert(content.begin(),
                   std::istreambuf_iterator<char>(fileStream),
                   std::istreambuf_iterator<char>());

    afterRead();

    switch (fd.type)
    {
    case FileDataType::Text:
    {
        UA_String data = UA_STRING_NULL;
        data.data=  reinterpret_cast<UA_Byte*>(content.data());
        data.length=content.size();//content.length();

        UA_Variant_setScalarCopy(&value->value, &data, &UA_TYPES[UA_TYPES_STRING]);

    }break;

    case FileDataType::Bool:
    {
        try{
            UA_Boolean data = (0 != std::stoi(content));
            UA_Variant_setScalarCopy(&value->value, &data, &UA_TYPES[UA_TYPES_BOOLEAN]);
        }catch(...){
            return UA_STATUSCODE_BADDATAUNAVAILABLE;
        }

    }break;

    case FileDataType::Number:
    {
        try{
            int data = std::stoi(content);
            UA_Variant_setScalarCopy(&value->value, &data, &UA_TYPES[UA_TYPES_INT32]);
        }catch(...){
            return UA_STATUSCODE_BADDATAUNAVAILABLE;
        }
    }break;

    case FileDataType::INVALID:
    default:

        std::clog << "exception while writing: " <<"\n";
        throw std::exception();
        break;
    }

    return UA_STATUSCODE_GOOD;
}

int GenericFileIO::onWriteNode(const UA_DataValue *newValue)
{
    if(!beforeWrite())
    {
        return UA_STATUSCODE_BADNODATAAVAILABLE;
    }

    switch (UaType2FileDataType(newValue->value.type))
    {
    case FileDataType::Text:
    {
        UA_String* val = (UA_String*)newValue->value.data;
        std::string_view txt{reinterpret_cast<const char*>(val->data), val->length};

        fileStream << txt ;

        std::clog << "text value written: " << txt <<"\n";
    }break;

    case FileDataType::Bool:
    {
        UA_Boolean* val = (UA_Boolean*)newValue->value.data;

        fileStream << (*val ? "1" : "0");

        std::clog << "bool value written: " << *val <<"\n";

    }break;

    case FileDataType::Number:
    {
        UA_Int32* val = (UA_Int32*)newValue->value.data;


        //        std::stringstream ss;
        //        ss << std::dec << *val;
        //        std::string s{ss.str()};
        fileStream << *val;;

        std::clog << "int value written: " << std::dec << (int)*val <<"\n";
    }break;

    case FileDataType::INVALID:
    default:

        std::clog << "exception while writing: " <<"\n";
        throw std::exception();
        break;
    }
    afterWrite();

    return  UA_STATUSCODE_GOOD;
}

int GenericFileIO::beforeRead()
{
    if(!fd.keep_open){
        if( !fileStream.is_open()){
            fileStream.open(fd.path.u8string().c_str(), mode );
        }
    }
    if(fileStream.good() && fileStream.is_open()){
        fileStream.seekg(0, std::ios::beg);
        return 1;
    }
    return 0;
  ;
}

int GenericFileIO::afterRead()
{
    if(!fd.keep_open)
        if(fileStream.is_open()){
            fileStream.close();
        }
    return fileStream.good();
}

int GenericFileIO::beforeWrite()
{
    if(fd.writable)
    {
        if(!fd.keep_open){
            auto m = mode | std::ios::trunc;
            if(!fileStream.is_open()){
                fileStream.open(fd.path.u8string().c_str(), m );
            }
        }
    }

    if(fileStream.good() && fileStream.is_open()){
        fileStream.seekg(0, std::ios::beg);
        return 1;
    }
    return 0;
}

int GenericFileIO::afterWrite()
{

    if(fd.writable)
    {
        if(!fd.keep_open)
        {
            if(fileStream.is_open()){
                fileStream.close();
            }
        }
        else{
            std::filesystem::resize_file(fd.path, fileStream.tellg() );
        }
    }
    return fileStream.good();
}

