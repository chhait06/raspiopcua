#include "GenericFileIO_Base.h"

#include "GenericFileIO.h"

#include <cpptoml.h>

#include <cerrno>
#include <string>

#include <string_view>
#include <vector>
#include <Log.h>


#define DEBUG_OUT(...)
#ifndef DEBUG_OUT
#define DEBUG_OUT(...) (std::cout << __VA_ARGS__ << "\n" );
#endif

struct ComponentApi getComponentApiHandle() {
    return getGenericFileIOComponenApi();
}

ComponentApi getGenericFileIOComponenApi()
{
    LOG(Log::Level::Informational, L_UNIT("GenericFileIO"), L_MESSAGE("Getting Api Handle"));
    struct ComponentApi cmp = { 0 };
    cmp.initFn = initFn;
    cmp.nodesFn = notesFn;
    cmp.startFn = startFn;
    cmp.iterateFn = iterFn;
    cmp.stopFn = stopFn;
    cmp.deleteFn = deleteFn;

    return  cmp;
}

int initFn(struct ComponentHandle * comp)
{
    DEBUG_OUT("file io - init")
            int ret{0};

    std::vector<GenericFileIO>* context = new std::vector<GenericFileIO>();
    comp->context = context;

    try {
        auto config = cpptoml::parse_file(comp->config_file);  //should not throw an exception. allready parsed in the server.

        auto files = config->get_table_array("file");

        for(const auto & f : *files)
        {
            auto type_text =f->get_as<std::string>("datatype");
            auto file_path = f->get_as<std::string>("path");
            auto writable = f->get_as<bool>("writable");
            auto keep_open= f->get_as<bool>("keep_open");
            //auto trunc_on_write= f->get_as<bool>("trunc_on_write");

            auto browse_name = f->get_as<std::string>("browsename");

            if(!file_path || !browse_name){
                break;
            }

            FileDescription fd
            {
                Text2FileDataType(type_text.value_or("Text"))
                ,std::filesystem::path{*file_path}
                ,writable.value_or(false)
                ,keep_open.value_or(true)
                ,(*browse_name)
            };
            context->push_back( { comp->server, fd} );
        }
    }
    catch (cpptoml::parse_exception& e)
    {
        deleteFn(comp);
        ret = ENOMSG;
    }
    catch(...)
    {
        deleteFn(comp);
        ret = ENOMSG;
    }
    return ret;
}

int notesFn(struct ComponentHandle * comp)
{
    DEBUG_OUT("file io - nodes")
            int ret{0};

    try {

    } catch (...){
        ret = 1;
    }
    return ret;
}

int startFn(struct ComponentHandle * comp)
{
    DEBUG_OUT("file io - start")
            int ret{0};
    std::vector<GenericFileIO>* context = static_cast<std::vector<GenericFileIO>*>( comp->context );
    if(!context){return 0;}
    try {
        for(auto& f : *context)
        {
            f.start();
        }

    } catch (...) {
        ret = 1;
    }
    return ret;
}

int iterFn(struct ComponentHandle * comp)
{
    // DEBUG_OUT("file io - iter")
    int ret{0};
    std::vector<GenericFileIO>* context = static_cast<std::vector<GenericFileIO>*>( comp->context );
    if(!context){return 0;}
    try {
        for(auto& f : *context)
        {
            f.iter();
        }

    } catch (...) {
        ret = 1;
    }
    return ret;
}

int stopFn(struct ComponentHandle * comp)
{
    DEBUG_OUT("file io - stop")
            int ret{0};
    std::vector<GenericFileIO>* context = static_cast<std::vector<GenericFileIO>*>( comp->context );
    if(!context){return 0;}
    try {
        for(auto& f : *context)
        {
            f.stop();
        }

    } catch (...) {
        ret = 1;
    }
    return ret;
}

int deleteFn(struct ComponentHandle * comp)
{
    DEBUG_OUT("file io - del " << (comp->context == nullptr ? "null" : "context" ))
            int ret{0};

    try {
        if(comp->context != nullptr)
        {
            delete static_cast<GenericFileIO*>(comp->context);
            comp->context = nullptr;
        }
    } catch (...) {
        ret = 1;
    }

    return ret;
}
