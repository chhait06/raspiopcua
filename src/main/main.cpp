﻿//#include <open62541.h>

#include <signal.h>
#include <string>

#include <functional>

#include <UaService.h>
#include <UaServer.h>
#include <ServiceConfigParser.h>

#include <Log.h>

using namespace hsesslingen::opcua;

ServiceConfig createServiceConfig(int argc, char** argv);
static void stopHandler(int sign);
extern std::function <void(void)> stopService;

std::function <void(void)> stopService = [](){};

#include <unistd.h>

int main(int argc, char** argv)
{
    signal(SIGINT, stopHandler); /* catches ctrl-c */
    signal(SIGTERM, stopHandler);

   LOG(Log::Level::Informational, L_UNIT("main:startup"), L_MESSAGE( "Starting Server..."), L_ELEM_F("PID","%d", getpid()));
    ServiceConfig serviceCfg{createServiceConfig(argc, argv)};

    UaService service(serviceCfg);

    service.init();

    stopService = [&service](){ service.stop(); };

    service.run();
    service.close();
}

static void stopHandler(int sign)
{
    switch (sign)
    {
    case SIGINT:
        LOG(Log::Level::Informational, L_UNIT("SIGNAL HANDLE"), L_MESSAGE( "Received SIGINT. Stopping..."), L_ELEM_F("SIGNAL_NUMBER","%d", sign));
        stopService();
        break;
    case SIGTERM:
        LOG(Log::Level::Informational, L_UNIT("SIGNAL HANDLE"), L_MESSAGE( "Received SIGTERM. Stopping..."), L_ELEM_F("SIGNAL_NUMBER","%d", sign));
        stopService();
        break;
    default:
        LOG(Log::Level::Informational, L_UNIT("SIGNAL HANDLE"), L_MESSAGE( "Received unkown signal. "), L_ELEM_F("SIGNAL_NUMBER", "%d", sign));

    }

}

ServiceConfig createServiceConfig(int argc, char** argv)
{
    ServiceConfig serviceCfg;

    if(argc < 2)
    {
        //default config
        LOG(Log::Level::Informational,
            L_UNIT_F("main:%s", "startup"),
            L_MESSAGE("Loading default config. No config file given"));

        return  serviceCfg;
    }
    //assuming argv[1] contains the config path

    auto config_path = std::filesystem::absolute(std::filesystem::path(argv[1]));

    if(! std::filesystem::is_regular_file(config_path))
    {
        LOG(Log::Level::Informational,
            L_UNIT_F("main:%s", "startup"),
            L_MESSAGE("Given config file not found"),
            L_ELEM("CONFIG_FILE_PATH",
                   config_path.u8string().c_str())
            );

        std::exit(404); //like http 404 Not Found
    }

    try
    {
        serviceCfg = ServiceConfigParser::ParseConfig(config_path);
    }
    catch(std::exception& e)
    {
        LOG(Log::Level::Informational,
            L_UNIT_F("main:%s", "startup"),
            L_MESSAGE("Error parsing given config file"),
            L_ELEM("CONFIG_FILE_PATH",
                   config_path.u8string().c_str()),
            L_ELEM("THROW_MESSAGE", e.what())
            );
        std::exit(415); //like HTTP 415 Unsupported Media Type
    }

    LOG(Log::Level::Informational,
        L_UNIT_F("main:%s", "startup"),
        L_MESSAGE("Successfully loaded config file"),
        L_ELEM("CONFIG_FILE_PATH",
               config_path.u8string().c_str())
        );

    return serviceCfg;
}


